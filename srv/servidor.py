#!/usr/bin/python
# -*- encoding: utf-8 -*-

from flask import Flask
from flask import request
from flask import render_template
import pandas as pd
import json

df_modificaciones = pd.read_csv('csv/modificacion_contrato/completo.csv', encoding='utf-8')
df_modificaciones['monto'].fillna(0, inplace=True)
df_modificaciones['vigencia_dias'].fillna(0, inplace=True)
df_contratos = pd.read_csv('csv/contratos/completo.csv', encoding='utf-8')
df_contratos['vigencia_dias'].fillna(0, inplace=True)
tmp = pd.merge(df_contratos, df_modificaciones, left_on='id', right_on='id_contrato', suffixes=['_c',''])

codigoToCategoria = {
    '0': 'Adquisición y Locación de inmuebles. Alquiler de muebles',
    '1': 'Bienes e insumos agropecuario y forestal',
    '2': 'Capacitaciones y Adiestramientos',
    '3': 'Combustibles y Lubricantes',
    '4': 'Construcción, Restauración, Reconstrucción o Remodelación y Reparación de Inmuebles',
    '5': 'Consultorías, Asesorías e Investigaciones. Estudios y Proyectos de inversión',
    '6': 'Elementos e insumos de limpieza',
    '7': 'Equipos Militares y de Seguridad. Servicio de Seguridad y Vigilancia',
    '8': 'Equipos, accesorios y programas computacionales, de oficina, educativos, de imprenta, de comunicación y señalamiento',
    '9': 'Equipos, Productos e instrumentales Médicos y de Laboratorio. Servicios asistenciales de salud',
    '10': 'Maquinarias, Equipos y herramientas mayores - Equipos de transporte',
    '11': 'Materiales e insumos eléctricos, metálicos y no metálicos, Plásticos, cauchos. Repuestos, herramientas, cámaras y cubiertas.',
    '12': 'Minerales',
    '13': 'Muebles y Enseres',
    '14': 'Pasajes y Transportes',
    '15': 'Productos Alimenticios',
    '16': 'Productos quimicos',
    '17': 'Publicidad y Propaganda',
    '18': 'Seguros',
    '19': 'Servicios de ceremonial, gastronomico y funerarios',
    '20': 'Servicios de Limpiezas, Mantenimientos y reparaciones menores y mayores de Instalaciones, Maquinarias y Vehículos',
    '21': 'Servicios Técnicos',
    '22': 'Textiles, vestuarios y calzados',
    '23': 'Utensilios de cocina y comedor, Productos de Porcelana, vidrio y loza',
    '24': 'Utiles de oficina, Productos de papel y cartón e impresos'
}

codigoToConvocante = {
    '1': 'Administración Nacional de Electricidad',
    '2': 'Administración Nacional de Navegación y Puertos',
    '3': 'Agencia Financiera de Desarrollo',
    '4': 'Agencia Nac. de Eval. y Acreditación de la Educación Sup.',
    '5': 'Agencia Nacional de Transito y Seguridad Vial',
    '6': 'Auditoria del Poder Ejecutivo / Presidencia de la República / PODER EJECUTIVO',
    '7': 'Banco Central del Paraguay',
    '8': 'Banco Nacional de Fomento',
    '9': 'Caja de Jubilaciones y Pensiones de Empl. de Bancos y Afines',
    '10': 'Caja de Jubilaciones y Pensiones del Personal de la Ande',
    '11': 'Caja de Jubilaciones y Pensiones del Personal Municipal',
    '12': 'Caja de Préstamos del Ministerio de Defensa Nacional',
    '13': 'Caja de Seguridad Social de Empleados y Obreros Ferroviarios',
    '14': 'Cámara de Diputados',
    '15': 'Cámara de Senadores',
    '16': 'Cañas Paraguayas S.A.',
    '17': 'Centro Cultural de la Republica - El Cabildo / Congreso Nacional / PODER LEGISLATIVO',
    '18': 'Colegio Experimental Paraguay - Brasil / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '19': 'Comisión Nac. de Prevención contra la Tortura y Otros Tratos',
    '20': 'Comisión Nacional de Telecomunicaciones',
    '21': 'Comisión Nacional de Valores',
    '22': 'Compañía Paraguaya de Comunicaciones',
    '23': 'Congreso Nacional',
    '24': 'Consejo de la Magistratura',
    '25': 'Consejo Nacional de Ciencia y Tecnología / Presidencia de la República / PODER EJECUTIVO',
    '26': 'Contraloría General de la República',
    '27': 'Corte Suprema de Justicia',
    '28': 'Crédito Agrícola de Habilitación',
    '29': 'Defensoría del Pueblo',
    '30': 'Defensoria Publica',
    '31': 'Dirección  de Beneficencia y Ayuda Social',
    '32': 'Dirección General de Estadisticas Encuestas y Censo / Presidencia de la República / PODER EJECUTIVO',
    '33': 'Direccion General de Informacion Estrategica de Salud Digies-Sinais / Ministerio de Salud Pública y Bienestar Social / PODER EJECUTIVO',
    '34': 'Direccion General de Migraciones / Ministerio del Interior / PODER EJECUTIVO',
    '35': 'Direccion General de Salud Ambiental / Ministerio de Salud Pública y Bienestar Social / PODER EJECUTIVO',
    '36': 'Direccion Nacional de Aduanas',
    '37': 'Dirección Nacional de Aeronaútica Civil',
    '38': 'Dirección Nacional de Contrataciones Públicas',
    '39': 'Dirección Nacional de Correos del Paraguay',
    '40': 'Direccion Nacional de Propiedad Intelectual',
    '41': 'Direccion Nacional de Transporte',
    '42': 'Empresa de Servicios Sanitarios del Paraguay',
    '43': 'Ente Regulador de Servicios Sanitarios',
    '44': 'Facultad de Arquitectura / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '45': 'Facultad de Ciencias Agrarias / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '46': 'Facultad de Ciencias Economicas / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '47': 'Facultad de Ciencias Exactas y Naturales / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '48': 'Facultad de Ciencias Medicas / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '49': 'Facultad de Ciencias Quimicas / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '50': 'Facultad de Ciencias Veterinarias / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '51': 'Facultad de Derecho y Ciencias Sociales / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '52': 'Facultad de Filosofia / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '53': 'Facultad de Ingenieria / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '54': 'Facultad de Odontologia / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '55': 'Facultad Politecnica / Universidad Nacional de Asunción / UNIVERSIDADES NACIONALES',
    '56': 'Fondo Ganadero',
    '57': 'Fondo Nacional de la Cultura y las Artes',
    '58': 'Gobierno Departamental de Alto Paraguay',
    '59': 'Gobierno Departamental de Alto Paraná',
    '60': 'Gobierno Departamental de Amambay',
    '61': 'Gobierno Departamental de Boquerón',
    '62': 'Gobierno Departamental de Caaguazú',
    '63': 'Gobierno Departamental de Caazapá',
    '64': 'Gobierno Departamental de Canindeyú',
    '65': 'Gobierno Departamental de Central',
    '66': 'Gobierno Departamental de Concepción',
    '67': 'Gobierno Departamental de Cordillera',
    '68': 'Gobierno Departamental de Guairá',
    '69': 'Gobierno Departamental de Itapua',
    '70': 'Gobierno Departamental de Misiones',
    '71': 'Gobierno Departamental de Ñeembucú',
    '72': 'Gobierno Departamental de Paraguarí',
    '73': 'Gobierno Departamental de Presidente Hayes',
    '74': 'Gobierno Departamental de San Pedro',
    '75': 'Hospital General Barrio Obrero / Ministerio de Salud Pública y Bienestar Social / PODER EJECUTIVO',
    '76': 'Hospital General Pediatrico Niños de Acosta Ñu / Ministerio de Salud Pública y Bienestar Social / PODER EJECUTIVO',
    '77': 'Hospital Nacional - Centro Medico Nacional / Ministerio de Salud Pública y Bienestar Social / PODER EJECUTIVO',
    '78': 'Industria Nacional del Cemento',
    '79': 'Instituto de Previsión Social',
    '80': 'Instituto Forestal Nacional',
    '81': 'Instituto Nacional de Cooperativismo',
    '82': 'Instituto Nacional de Desarrollo Rural y de la Tierra',
    '83': 'Instituto Nacional del Cancer / Ministerio de Salud Pública y Bienestar Social / PODER EJECUTIVO',
    '84': 'Instituto Nacional de Tecnología, Normalización y Metrologia',
    '85': 'Instituto Paraguayo de Artesania',
    '86': 'Instituto Paraguayo del Indígena',
    '87': 'Instituto Paraguayo de Tecnologia Agraria',
    '88': 'Jurado de Enjuiciamiento de Magistrados',
    '89': 'Justicia Electoral',
    '90': 'Loma Plata',
    '91': 'Ministerio de Agricultura y Ganadería',
    '92': 'Ministerio de Defensa Nacional',
    '93': 'Ministerio de Educación y Cultura',
    '94': 'Ministerio de Hacienda',
    '95': 'Ministerio de Industria y Comercio',
    '96': 'Ministerio de Justicia',
    '97': 'Ministerio de la Mujer',
    '98': 'Ministerio del Interior',
    '99': 'Ministerio de Obras Públicas y Comunicaciones',
    '100': 'Ministerio de Relaciones Exteriores',
    '101': 'Ministerio de Salud Pública y Bienestar Social',
    '102': 'Ministerio de Trabajo, Empleo y Seguridad Social',
    '103': 'Ministerio Público',
    '104': 'Municipalidad de 3 de Febrero',
    '105': 'Municipalidad de 3 de Mayo',
    '106': 'Municipalidad de Acahay',
    '107': 'Municipalidad de Altos',
    '108': 'Municipalidad de Alto Verá',
    '109': 'Municipalidad de Antequera',
    '110': 'Municipalidad de Aregua',
    '111': 'Municipalidad de Arroyos y Esteros',
    '112': 'Municipalidad de Asunción',
    '113': 'Municipalidad de Atyra',
    '114': 'Municipalidad de Avaí',
    '115': 'Municipalidad de Ayolas',
    '116': 'Municipalidad de Bahia Negra',
    '117': 'Municipalidad de Belén',
    '118': 'Municipalidad de Bella Vista - Amambay',
    '119': 'Municipalidad de Bella Vista - Itapúa',
    '120': 'Municipalidad de Benjamín Aceval',
    '121': 'Municipalidad de Borja',
    '122': 'Municipalidad de Buena Vista',
    '123': 'Municipalidad de Caacupé',
    '124': 'Municipalidad de Caaguazú',
    '125': 'Municipalidad de Caapucú',
    '126': 'Municipalidad de Caazapá',
    '127': 'Municipalidad de Cambyretá',
    '128': 'Municipalidad de Capiatá',
    '129': 'Municipalidad de Capiibary',
    '130': 'Municipalidad de Capitán Meza',
    '131': 'Municipalidad de Capitán Miranda',
    '132': 'Municipalidad de Caraguatay',
    '133': 'Municipalidad de Carapeguá',
    '134': 'Municipalidad de Carayaó',
    '135': 'Municipalidad de Cárlos Antonio López',
    '136': 'Municipalidad de Carmelo Peralta',
    '137': 'Municipalidad de Carmen del Paraná',
    '138': 'Municipalidad de Cerrito',
    '139': 'Municipalidad de Choré',
    '140': 'Municipalidad de Ciudad del Este',
    '141': 'Municipalidad de Concepción',
    '142': 'Municipalidad de Coronel Bogado',
    '143': 'Municipalidad de Coronel Martínez',
    '144': 'Municipalidad de Coronel Oviedo',
    '145': 'Municipalidad de Corpus Christi',
    '146': 'Municipalidad de Desmochados',
    '147': 'Municipalidad de Domingo Martínez de Irala',
    '148': 'Municipalidad de Dr. Bottrell',
    '149': 'Municipalidad de Dr. Raul Peña',
    '150': 'Municipalidad de Edelira',
    '151': 'Municipalidad de Emboscada',
    '152': 'Municipalidad de Encarnación',
    '153': 'Municipalidad de Escobar',
    '154': 'Municipalidad de Eusebio Ayala',
    '155': 'Municipalidad de Félix Pérez Cardozo',
    '156': 'Municipalidad de Fernando de la Mora',
    '157': 'Municipalidad de Filadelfia',
    '158': 'Municipalidad de Fram',
    '159': 'Municipalidad de Fuerte Olimpo',
    '160': 'Municipalidad de Fulgencio Yegros',
    '161': 'Municipalidad de General Artigas',
    '162': 'Municipalidad de General Delgado',
    '163': 'Municipalidad de Gral. Bernardino Caballero',
    '164': 'Municipalidad de Gral. Elizardo Aquino',
    '165': 'Municipalidad de Gral. Eugenio A. Garay',
    '166': 'Municipalidad de Gral. Francisco Caballero Alvarez',
    '167': 'Municipalidad de Gral. Francisco I. Resquin',
    '168': 'Municipalidad de Gral. Higinio Morínigo',
    '169': 'Municipalidad de Gral. José Eduvigis Díaz',
    '170': 'Municipalidad de Gral Jose M. Bruguez',
    '171': 'Municipalidad de Guajayvi',
    '172': 'Municipalidad de Guarambaré',
    '173': 'Municipalidad de Guazu Cuá',
    '174': 'Municipalidad de Hernandarias',
    '175': 'Municipalidad de Hohenau',
    '176': 'Municipalidad de Humaitá',
    '177': 'Municipalidad de Independencia',
    '178': 'Municipalidad de Iruña',
    '179': 'Municipalidad de Isla Pucú',
    '180': 'Municipalidad de Isla Umbú',
    '181': 'Municipalidad de Itá',
    '182': 'Municipalidad de Itacurubí de las Cordilleras',
    '183': 'Municipalidad de Itacurubí del Rosario',
    '184': 'Municipalidad de Itakyry',
    '185': 'Municipalidad de Itanará',
    '186': 'Municipalidad de Itapé',
    '187': 'Municipalidad de Itapúa Poty',
    '188': 'Municipalidad de Itaugua',
    '189': 'Municipalidad de Iturbe',
    '190': 'Municipalidad de J. Augusto Saldivar',
    '191': 'Municipalidad de Jesús',
    '192': 'Municipalidad de J. Eulogio Estigarribia',
    '193': 'Municipalidad de José Domingo Ocampos',
    '194': 'Municipalidad de José Falcón',
    '195': 'Municipalidad de José Leandro Oviedo',
    '196': 'Municipalidad de Juan de Mena',
    '197': "Municipalidad de Juan E. O'Leary",
    '198': 'Municipalidad de Juan León Mallorquín',
    '199': 'Municipalidad de Juan Manuel Frutos',
    '200': 'Municipalidad de Katuete',
    '201': 'Municipalidad de la Colmena',
    '202': 'Municipalidad de Lambaré',
    '203': 'Municipalidad de la Paloma del Espiritu Santo',
    '204': 'Municipalidad de la Paz',
    '205': 'Municipalidad de Liberacion',
    '206': 'Municipalidad de Lima',
    '207': 'Municipalidad de Limpio',
    '208': 'Municipalidad de Loma Grande',
    '209': 'Municipalidad de Loreto',
    '210': 'Municipalidad de los Cedrales',
    '211': 'Municipalidad de Luque',
    '212': 'Municipalidad de Maciel',
    '213': 'Municipalidad de Mariano Roque Alonso',
    '214': 'Municipalidad de Mayor José D. Martinez',
    '215': 'Municipalidad de Mayor Otaño',
    '216': 'Municipalidad de Mbaracayú',
    '217': 'Municipalidad de Mbocayaty del Guairá',
    '218': 'Municipalidad de Mbocayaty del Yhaguy',
    '219': 'Municipalidad de Mbuyapey',
    '220': 'Municipalidad de Mcal. Francisco Solano López',
    '221': 'Municipalidad de Mcal Jose F. Estigarribia',
    '222': 'Municipalidad de Minga Guazú',
    '223': 'Municipalidad de Minga Porá',
    '224': 'Municipalidad de Ñacunday',
    '225': 'Municipalidad de Naranjal',
    '226': 'Municipalidad de Natalio',
    '227': 'Municipalidad de Ñemby',
    '228': 'Municipalidad de Nueva Alborada',
    '229': 'Municipalidad de Nueva Colombia',
    '230': 'Municipalidad de Nueva Esperanza',
    '231': 'Municipalidad de Nueva Germania',
    '232': 'Municipalidad de Nueva Italia',
    '233': 'Municipalidad de Ñumi',
    '234': 'Municipalidad de Obligado',
    '235': 'Municipalidad de Paraguarí',
    '236': 'Municipalidad de Paso Barreto',
    '237': 'Municipalidad de Paso de Patria',
    '238': 'Municipalidad de Paso Yobai',
    '239': 'Municipalidad de Pedro Juan Caballero',
    '240': 'Municipalidad de Pilar',
    '241': 'Municipalidad de Pirapó',
    '242': 'Municipalidad de Pirayú',
    '243': 'Municipalidad de Piribebuy',
    '244': 'Municipalidad de Presidente Franco',
    '245': 'Municipalidad de Primero de Marzo',
    '246': 'Municipalidad de Puerto Pinasco',
    '247': 'Municipalidad de Quiindy',
    '248': 'Municipalidad de Quyquyho',
    '249': 'Municipalidad de Raúl Arsenio Oviedo',
    '250': 'Municipalidad de Repatriación',
    '251': 'Municipalidad de R.I. 3 Corrales',
    '252': 'Municipalidad de Salto del Guairá',
    '253': 'Municipalidad de San Alberto',
    '254': 'Municipalidad de San Antonio',
    '255': 'Municipalidad de San Bernardino',
    '256': 'Municipalidad de San Cosme y Damián',
    '257': 'Municipalidad de San Cristobal',
    '258': 'Municipalidad de San Estanislao',
    '259': 'Municipalidad de San Ignacio',
    '260': 'Municipalidad de San Joaquín',
    '261': 'Municipalidad de San José de los Arroyos',
    '262': 'Municipalidad de San Juan Bautista',
    '263': 'Municipalidad de San Juan Bautista Ñeembucu',
    '264': 'Municipalidad de San Juan del Paraná',
    '265': 'Municipalidad de San Juan Nepomuceno',
    '266': 'Municipalidad de San Lázaro',
    '267': 'Municipalidad de San Lorenzo',
    '268': 'Municipalidad de San Miguel',
    '269': 'Municipalidad de San Pablo',
    '270': 'Municipalidad de San Pedro del Paraná',
    '271': 'Municipalidad de San Pedro del Ycuamandiyú',
    '272': 'Municipalidad de San Rafael del Paraná',
    '273': 'Municipalidad de San Salvador',
    '274': 'Municipalidad de Santa Elena',
    '275': 'Municipalidad de Santa Fé del Paraná',
    '276': 'Municipalidad de Santa María',
    '277': 'Municipalidad de Santa Rita',
    '278': 'Municipalidad de Santa Rosa del Aguaray',
    '279': 'Municipalidad de Santa Rosa del Mbutuy',
    '280': 'Municipalidad de Santa Rosa del Monday',
    '281': 'Municipalidad de Santa Rosa Misiones',
    '282': 'Municipalidad de Santiago',
    '283': 'Municipalidad de Sapucai',
    '284': 'Municipalidad de Simón Bolivar',
    '285': 'Municipalidad de Tacuaras',
    '286': 'Municipalidad de Tavaí',
    '287': 'Municipalidad de Tavapy',
    '288': 'Municipalidad de Tebicuary',
    '289': 'Municipalidad de Tebicuarymi',
    '290': 'Municipalidad de Tembiapora',
    '291': 'Municipalidad de Tobatí',
    '292': 'Municipalidad de Tomás Romero Pereira',
    '293': 'Municipalidad de Trinidad',
    '294': 'Municipalidad de Tte Esteban Martinez',
    '295': 'Municipalidad de Unión',
    '296': 'Municipalidad de Valenzuela',
    '297': 'Municipalidad de Vaquería',
    '298': 'Municipalidad de Villa Curuguaty',
    '299': 'Municipalidad de Villa del Rosario',
    '300': 'Municipalidad de Villa Elisa',
    '301': 'Municipalidad de Villa Florida',
    '302': 'Municipalidad de Villa Hayes',
    '303': 'Municipalidad de Villalbín',
    '304': 'Municipalidad de Villa Oliva',
    '305': 'Municipalidad de Villarrica',
    '306': 'Municipalidad de Villa Ygatimí',
    '307': 'Municipalidad de Villeta',
    '308': 'Municipalidad de Yaguaron',
    '309': 'Municipalidad de Yasy Cañy',
    '310': 'Municipalidad de Yataity del Norte',
    '311': 'Municipalidad de Yatytay',
    '312': 'Municipalidad de Ybycui',
    '313': 'Municipalidad de Ybyrarobana',
    '314': 'Municipalidad de Ybytimi',
    '315': 'Municipalidad de Yby Yaú',
    '316': 'Municipalidad de Yguazú',
    '317': 'Municipalidad de Yhú',
    '318': 'Municipalidad de Ypacarai',
    '319': 'Municipalidad de Ypané',
    '320': 'Municipalidad de Ypejhu',
    '321': 'Municipalidad de Yrybucuá',
    '322': 'Municipalidad de Yuty',
    '323': 'Municipalidad de Zanja Pyta',
    '324': 'Petróleos Paraguayos',
    '325': 'Policia Nacional / Ministerio del Interior / PODER EJECUTIVO',
    '326': 'Presidencia de la República',
    '327': 'Procuraduria General de la Republica / Presidencia de la República / PODER EJECUTIVO',
    '328': 'Programa Ampliado de Inmunizaciones  / Ministerio de Salud Pública y Bienestar Social / PODER EJECUTIVO',
    '329': 'Secretaria de Accion Social / Presidencia de la República / PODER EJECUTIVO',
    '330': 'Secretaria de Defensa del Consumidor y el Usuario',
    '331': 'Secretaria de Emergencia Nacional / Presidencia de la República / PODER EJECUTIVO',
    '332': 'Secretaria de Información y Comunicación para el Desarrollo / Presidencia de la República / PODER EJECUTIVO',
    '333': 'Secretaria de la Funcion Publica / Presidencia de la República / PODER EJECUTIVO',
    '334': 'Secretaría del Ambiente',
    '335': 'Secretaria de Prevención de Lavado de Dinero / Presidencia de la República / PODER EJECUTIVO',
    '336': 'Secretaría de Transporte Área Metropolitana de Asuncion',
    '337': 'Secretaria Nacional Anticorrupcion / Presidencia de la República / PODER EJECUTIVO',
    '338': 'Secretaria Nacional de Cultura / Presidencia de la República / PODER EJECUTIVO',
    '339': 'Secretaria Nacional de la Juventud  / Presidencia de la República / PODER EJECUTIVO',
    '340': 'Secretaria Nacional de la Niñez y la Adolescencia / Presidencia de la República / PODER EJECUTIVO',
    '341': 'Secretaria Nacional de la Vivienda y el Habitat',
    '342': 'Secretaria Nacional de Tecnologias de la Informacion y Comunicacion  / Presidencia de la República / PODER EJECUTIVO',
    '343': 'Secretaria Nacional de Turismo / Presidencia de la República / PODER EJECUTIVO',
    '344': 'Secretaria Nacional por los Derechos Humanos de las Personas con Discapacidad / Presidencia de la República / PODER EJECUTIVO',
    '345': 'Secretaria Tecnica de Planificacion / Presidencia de la República / PODER EJECUTIVO',
    '346': 'Servicio Nacional Antidrogas / Presidencia de la República / PODER EJECUTIVO',
    '347': 'Servicio Nacional de Calidad y Salud Animal',
    '348': 'Servicio Nacional de Calidad y Sanidad Vegetal y de Semillas',
    '349': 'Servicio Nacional de Promocion Profesional  / Ministerio de Trabajo, Empleo y Seguridad Social / PODER EJECUTIVO',
    '350': 'Sistema Nacional de Formación y Capacitación Laboral / Ministerio de Trabajo, Empleo y Seguridad Social / PODER EJECUTIVO',
    '351': 'Subsecretaria de Estado de Tributación / Ministerio de Hacienda / PODER EJECUTIVO',
    '352': 'Tte Irala Fernandez',
    '353': 'Universidad Nacional de Asunción',
    '354': 'Universidad Nacional de Caaguazu',
    '355': 'Universidad Nacional de Canindeyu',
    '356': 'Universidad Nacional de Concepción',
    '357': 'Universidad Nacional de Itapúa',
    '358': 'Universidad Nacional del Este',
    '359': 'Universidad Nacional de Pilar',
    '360': 'Universidad Nacional de Villarrica del Espiritu Santo',
    '361': 'Vicepresidencia de la República'
}

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/coincidencias')
def coincidencias():
    id = request.args.get('id')
    if id is None: id = ''
    if id:
        # Filtros
        tmp_contratos = df_contratos[df_contratos.id.str.contains(id)].copy()
        return tmp_contratos.id.head(10).to_json(orient='records')

@app.route('/contratos')
def contratos():
    id = request.args.get('id')
    if id is None: id = ''
    if id:
        # Filtros
        tmp_contratos = df_contratos[df_contratos.id.str.contains(id)].copy()
        return tmp_contratos.to_json(orient='records')
    else:
        return 'Debe indicar al menos un contrato'

@app.route('/modificaciones', methods=['GET','POST'])
def modificaciones():
    tmp_modificaciones = pd.merge(df_contratos, df_modificaciones, left_on='id', right_on='id_contrato', suffixes=['_c',''])
    # Para el primer grafico de Modificaciones
    if request.method == 'POST':
        id_contrato = categoria = cantidad = anio = convocante = ''
        for parametros in request.form:
            params = json.loads(parametros)
            id_contrato = params['id_contrato']
            categoria = params['categoria']
            cantidad = int(params['cantidad'])
            anio = params['anio']
            convocante = params['convocantes']

        if categoria is None: categoria = []
        if anio is None: anio = []
        if convocante is None: convocante = []
        # Filtros
        if id_contrato:
            tmp_modificaciones = tmp_modificaciones[tmp_modificaciones.id_contrato.str.contains(id_contrato)]
        if len(categoria):
            for i in range(0,len(categoria)):
                categoria[i] = codigoToCategoria[categoria[i]]
            tmp_modificaciones = tmp_modificaciones[tmp_modificaciones.categoria.isin(categoria)]
        if len(convocante):
            for i in range(0,len(convocante)):
                convocante[i] = codigoToConvocante[convocante[i]]
            tmp_modificaciones = tmp_modificaciones[tmp_modificaciones.convocante.isin(convocante)]
        if len(anio):
            for i in range(0,len(anio)):
                anio[i] = int(anio[i])
            tmp_modificaciones = tmp_modificaciones[tmp_modificaciones.anio.isin(anio)]
        if cantidad:
            tmp_modificaciones_top_montos = tmp_modificaciones[tmp_modificaciones._tipo == 'AMP_MONTO'].sort(('monto'),ascending=False).head(cantidad)
            tmp_modificaciones_top_plazos = tmp_modificaciones[tmp_modificaciones._tipo == 'AMP_PLAZO'].sort(('vigencia_dias'),ascending=False).head(cantidad)
            tmp_modificaciones = pd.concat([tmp_modificaciones_top_montos, tmp_modificaciones_top_plazos])
        return tmp_modificaciones.to_json(orient='records')
    else: # Para la construccion de los graficos de montos, plazos y gantt
        id_contrato = request.args.get('id_contrato')
        tmp_modificaciones = tmp_modificaciones[tmp_modificaciones.id_contrato == id_contrato]
        return tmp_modificaciones.to_json(orient='records')

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=80)
