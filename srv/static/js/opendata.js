var opendata = {
    graficos: {}
};

opendata.graficos.utils = {
    separadorMiles: function (num) {
        return parseInt(num).toLocaleString()
    },
    sumaFecha: function (fecha, d)
    {
        var fechaFormateada = moment(fecha).format("DD/MM/YYYY")
        var Fecha = new Date();
        var sFecha = fechaFormateada || (Fecha.getDate() + "/" + (Fecha.getMonth() + 1) + "/" + Fecha.getFullYear());
        var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
        var aFecha = sFecha.split(sep);
        var fecha = aFecha[2] + '/' + aFecha[1] + '/' + aFecha[0];
        fecha = new Date(fecha);
        fecha.setDate(fecha.getDate() + parseInt(d));
        var anno = fecha.getFullYear();
        var mes = fecha.getMonth() + 1;
        var dia = fecha.getDate();
        mes = (mes < 10) ? ("0" + mes) : mes;
        dia = (dia < 10) ? ("0" + dia) : dia;
        var fechaFinal = mes + sep + dia + sep + anno;
        return (fechaFinal);
    },
    'colorTipoMap': {
        'CONTRATO': '#1f77b4',
        'AMP_PLAZO': 'orange',
        'AMP_MONTO': 'green'
    },
    'anioCambio': {
        '2010': 4741,
        '2011': 4190,
        '2012': 4425,
        '2013': 4311,
        '2014': 4477,
        '2015': 4784
    },
    'codigoToDescripcion': {
        'mes': {
            '1': 'Enero',
            '2': 'Febrero',
            '3': 'Marzo',
            '4': 'Abril',
            '5': 'Mayo',
            '6': 'Junio',
            '7': 'Julio',
            '8': 'Agosto',
            '9': 'Setiembre',
            '10': 'Octubre',
            '11': 'Noviembre',
            '12': 'Diciembre'
        },
        'convocante': {},
        'categoria': {
            '0': 'Adquisición y Locación de inmuebles. Alquiler de muebles',
            '1': 'Bienes e insumos agropecuario y forestal',
            '2': 'Capacitaciones y Adiestramientos',
            '3': 'Combustibles y Lubricantes',
            '4': 'Construcción, Restauración, Reconstrucción o Remodelación y Reparación de Inmuebles',
            '5': 'Consultorías, Asesorías e Investigaciones. Estudios y Proyectos de inversión',
            '6': 'Elementos e insumos de limpieza',
            '7': 'Equipos Militares y de Seguridad. Servicio de Seguridad y Vigilancia',
            '8': 'Equipos, accesorios y programas computacionales, de oficina, educativos, de imprenta, de comunicación y señalamiento',
            '9': 'Equipos, Productos e instrumentales Médicos y de Laboratorio. Servicios asistenciales de salud',
            '10': 'Maquinarias, Equipos y herramientas mayores - Equipos de transporte',
            '11': 'Materiales e insumos eléctricos, metálicos y no metálicos, Plásticos, cauchos. Repuestos, herramientas, cámaras y cubiertas.',
            '12': 'Minerales',
            '13': 'Muebles y Enseres',
            '14': 'Pasajes y Transportes',
            '15': 'Productos Alimenticios',
            '16': 'Productos quimicos',
            '17': 'Publicidad y Propaganda',
            '18': 'Seguros',
            '19': 'Servicios de ceremonial, gastronomico y funerarios',
            '20': 'Servicios de Limpiezas, Mantenimientos y reparaciones menores y mayores de Instalaciones, Maquinarias y Vehículos',
            '21': 'Servicios Técnicos',
            '22': 'Textiles, vestuarios y calzados',
            '23': 'Utensilios de cocina y comedor, Productos de Porcelana, vidrio y loza',
            '24': 'Utiles de oficina, Productos de papel y cartón e impresos'
        },
        '_tipo': {
            'CONTRATO': 'Contrato',
            'AMP_MONTO': 'Ampliación de monto',
            'AMP_PLAZO': 'Ampliación de plazo',
        },
        'moneda': {
            'USD': 'Dólares Americanos',
            'PYG': 'Guaraníes'
        }
    },
    'criterioToColor': {
        '_moneda': {
            'USD': '#1f77b4',
            'PYG': 'red'
        },
        '_tipo': {
            'AMP_PLAZO': 'orange',
            'AMP_MONTO': 'green'
        }
    },
    'tooltip': {},
    'mostrarTooltip': function (nodo) {
        var moneda;
        if (nodo._moneda == 'USD') {
            moneda = nodo._moneda;
        } else {
            moneda = 'Gs.';
        }
        if (this.esContrato(nodo)) {
            return  '<p style="text-align: center;"><strong>Contrato</strong></p><br>' +
                    '<strong>Identificador: </strong> ' + nodo.id + '<br>' +
                    '<strong>Proveedor: </strong> ' + nodo.razon_social + '<br>' +
                    '<strong>Llamado: </strong> ' + nodo.nombre_licitacion + '<br>' +
                    '<strong>Convocante:  </strong> ' + nodo.convocante + '<br>' +
                    '<strong>Categoria:  </strong> ' + nodo.categoria + '<br>' +
                    '<strong>Tipo de procedimiento: </strong> ' + nodo.tipo_procedimiento + '<br>' +
                    '<strong>Fecha firma contrato: </strong> ' + moment(nodo.fecha_firma_contrato.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    //'<strong>Fecha inicio: </strong> ' + moment(nodo.startDate.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Fecha fin del contrato: </strong> ' + moment(nodo.endDate.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Monto original del contrato: </strong> ' + opendata.graficos.utils.separadorMiles(nodo.monto_adjudicado) + ' ' + moneda;
        } else {
            return  '<p style="text-align: center;"><strong>Modificación de Contrato</strong></p><br>' +
                    '<strong>Identificador: </strong> ' + nodo.id + '<br>' +
                    '<strong>Convocante:  </strong> ' + nodo.convocante + '<br>' +
                    '<strong>Proveedor: </strong> ' + nodo.razon_social + '<br>' +
                    '<strong>Categoria:  </strong> ' + nodo.categoria + '<br>' +
                    '<strong>Tipo de Modificación: </strong> ' + nodo.tipo + '<br>' +
                    '<strong>Fecha firma de Modificación: </strong> ' + moment(nodo.fecha_contrato.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Cantidad de días agregados: </strong> ' + nodo.vigencia_dias + ' dias<br>' +
                    //'<strong>Fecha inicio de la modificación: </strong> ' + moment(nodo.startDate.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Fecha fin de la modificación en base a la cantidad de días agregados: </strong> ' + moment(nodo.endDate.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Monto de la modificación: </strong> ' + opendata.graficos.utils.separadorMiles(nodo.monto) + ' ' + moneda;
        }
    },
    'esContrato': function (nodo) {
        return (nodo._tipo == 'CONTRATO');
    },
    'mouseover': function (nodo) {
        this.tooltip.html(this.mostrarTooltip(nodo))
                .style('visibility', 'visible')
                .style('stroke-width', '2px');
    },
    'mouseout': function () {
        this.tooltip.style('visibility', 'hidden')
                .style('stroke-width', '0.5px');
    },
    'mousemove': function () {
        this.tooltip.style('top',
                (d3.event.pageY - 10) + 'px').style('left', (d3.event.pageX + 10) + 'px');
    }
};
opendata.graficos.globals = {
    'select2_id_contrato': '#id_contrato',
    'select2_categoria_contrato': '#categoria',
    'combobox_cantidad_modificaciones': '#cantidad',
    'select2_anio': '#anio',
    'select2_convocante': '#convocante',
    'btn_agrupar': '#agrupar_por',
    'btn_color': '#color',
    'grafico_modificaciones': '#chart1',
    'titulo_montos': '#tituloChart2',
    'grafico_montos': '#chart2',
    'titulo_plazos': '#tituloChart3',
    'grafico_plazos': '#chart3',
    'titulo_gantt': '#tituloChart4',
    'grafico_gantt': '#chart4',
    'leyenda_colores': '#leyenda',
    'titulo_contrato': '#titulo_contrato'
};
opendata.graficos.montos = {
    'ancho': function () {
        return parseInt(window.innerWidth / 4) || 0;
    },
    'alto': function () {
        return parseInt(window.innerWidth / 4) || 0;
    },
    graficar: function (id_contrato) {
        $(opendata.graficos.globals.titulo_montos).html('<h3 align="center"><label>Proporción por montos</label></h3>');
        $(opendata.graficos.globals.grafico_montos).empty();
        /* Recuperamos los datos del contrato. Debemos obtener en la columna value el plazo correspondiente al contrato */
        d3.json('/contratos?id=' + id_contrato, function (contrato) {
            if (contrato.length) {
                d3.json('/modificaciones?id_contrato=' + id_contrato, function (modificaciones) {
                    /* Si se trajo algun registro */
                    if (modificaciones.length) {
                        function mouseover(p) {
                            opendata.graficos.utils.mouseover(p.data);
                        }
                        function mouseout() {
                            opendata.graficos.utils.mouseout();
                        }
                        function mousemove() {
                            opendata.graficos.utils.mousemove()
                        }
                        /* Radio de la dona relativo al tamanio del lienzo para el grafico de Montos */
                        var r = parseInt(opendata.graficos.montos.ancho() * 2 / 5);
                        contrato[0].value = parseInt(contrato[0].monto_adjudicado);

                        if (contrato[0]._moneda == 'PYG') {
                            s = contrato[0].value;
                        } else {
                            s = contrato[0].value * opendata.graficos.utils.anioCambio[contrato[0].anio];
                        }

                        for (var i in modificaciones) {
                            /* Se agrega el campo value para identificar el monto original y el transformado en caso de modificaciones en USD */
                            modificaciones[i].value = parseInt(modificaciones[i].monto);
                            if (modificaciones[i]._moneda != 'PYG') {
                                modificaciones[i].value = modificaciones[i].value * opendata.graficos.utils.anioCambio[modificaciones[i].anio];
                            }
                            s = s + modificaciones[i].value;
                        }

                        var data_para_graficar = contrato.concat(modificaciones);

                        var color = d3.scale.category10();

                        var canvas = d3.select(opendata.graficos.globals.grafico_montos)
                                .append('svg')
                                .attr('width', opendata.graficos.montos.ancho())
                                .attr('height', opendata.graficos.montos.alto())
                                .style('display', 'block')
                                .style('margin', 'auto');

                        var group = canvas.append('g')
                                .attr('transform', 'translate(' + opendata.graficos.montos.ancho() / 2 + ',' + opendata.graficos.montos.alto() / 2 + ')')

                        var arc = d3.svg.arc()
                                .innerRadius(parseInt(r / 2))
                                .outerRadius(r);

                        var pie = d3.layout.pie()
                                .value(function (d) {
                                    return d.value;
                                });

                        var arcs = group.selectAll('.arc')
                                .data(pie(data_para_graficar))
                                .enter()
                                .append('g')
                                .attr('class', 'arc');

                        arcs.append('path')
                                .attr('d', arc)
                                .attr('fill', function (d) {
                                    return opendata.graficos.utils.colorTipoMap[d.data._tipo];
                                });

                        arcs.append('text')
                                .attr('transform', function (d) {
                                    return 'translate(' + arc.centroid(d) + ')'
                                })
                                .attr('text-anchor', 'middle')
                                .attr('font-size', '.8em')
                                .text(function (d) {
                                    if (d.data.value) {
                                        if (d.endAngle - d.startAngle > .8) {
                                            return parseInt(d.data.value).toLocaleString();
                                        }
                                    }
                                });

                        arcs.style('cursor', 'pointer')
                                .style('stroke', 'black')
                                .style('stroke-width', 0.5)
                                .on('mouseover', mouseover)
                                .on('mouseout', mouseout)
                                .on('mousemove', mousemove);

                        canvas.append('text')
                                .attr('transform', 'translate(' + opendata.graficos.montos.ancho() / 2 + ',' + opendata.graficos.montos.alto() / 2 + ')')
                                .text(parseInt(s).toLocaleString() + ' Gs.')
                                .attr('font-size', '.9em')
                                .attr('text-anchor', 'middle');
                    } else {
                        alert('No existen modificaciones de montos para este contrato');
                    }

                });
            } else {
                alert('No existe registro para el identificador de contrato proveido');
            }
        });
    }
};
opendata.graficos.plazos = {
    'ancho': function () {
        return parseInt(window.innerWidth / 4) || 0;
    },
    'alto': function () {
        return parseInt(window.innerWidth / 4) || 0;
    },
    graficar: function (id_contrato) {
        $(opendata.graficos.globals.titulo_plazos).html('<h3 align="center"><label>Proporción por plazos</label></h3>');
        $(opendata.graficos.globals.grafico_plazos).empty();

        /* Recuperamos los datos del contrato. Debemos obtener en la columna value el plazo correspondiente al contrato */
        d3.json('/contratos?id=' + id_contrato, function (contrato) {
            if (contrato.length) {
                d3.json('/modificaciones?id_contrato=' + id_contrato, function (modificaciones) {
                    /* Si se trajo algun registro */
                    if (modificaciones.length) {
                        function mouseover(p) {
                            opendata.graficos.utils.mouseover(p.data);
                        }
                        function mouseout() {
                            opendata.graficos.utils.mouseout();
                        }
                        function mousemove() {
                            opendata.graficos.utils.mousemove()
                        }
                        var data_para_graficar = contrato.concat(modificaciones);
                        var r = parseInt(opendata.graficos.plazos.ancho() * 2 / 5);
                        var s = 0;
                        for (var i in data_para_graficar) {
                            data_para_graficar[i].value = parseInt(data_para_graficar[i].vigencia_dias)
                            s = s + data_para_graficar[i].value;
                        }

                        var color = d3.scale.category10();

                        var canvas = d3.select(opendata.graficos.globals.grafico_plazos)
                                .append('svg')
                                .attr('width', opendata.graficos.plazos.ancho())
                                .attr('height', opendata.graficos.plazos.alto())
                                .style('display', 'block')
                                .style('margin', 'auto');

                        var group = canvas.append('g')
                                .attr('transform', 'translate(' + opendata.graficos.plazos.ancho() / 2 + ',' + opendata.graficos.plazos.alto() / 2 + ')')

                        var arc = d3.svg.arc()
                                .innerRadius(parseInt(opendata.graficos.plazos.ancho() / 5))
                                .outerRadius(r);

                        var pie = d3.layout.pie()
                                .value(function (d) {
                                    return d.value;
                                });

                        var arcs = group.selectAll('.arc')
                                .data(pie(data_para_graficar))
                                .enter()
                                .append('g')
                                .attr('class', 'arc');

                        arcs.append('path')
                                .attr('d', arc)
                                .attr('fill', function (d) {
                                    return opendata.graficos.utils.colorTipoMap[d.data._tipo];
                                });

                        arcs.append('text')
                                .attr('transform', function (d) {
                                    return 'translate(' + arc.centroid(d) + ')'
                                })
                                .attr('text-anchor', 'middle')
                                .attr('font-size', '.8em')
                                .text(function (d) {
                                    if (d.data.value) {
                                        if (d.endAngle - d.startAngle > .2) {
                                            return parseInt(d.data.value).toLocaleString();
                                        }
                                    }
                                });

                        arcs.attr('cursor', 'pointer')
                                .style('stroke', 'black')
                                .style('stroke-width', 0.5)
                                .on('mouseover', mouseover)
                                .on('mouseout', mouseout)
                                .on('mousemove', mousemove);

                        canvas.append('text')
                                .attr('transform', 'translate(' + opendata.graficos.plazos.ancho() / 2 + ',' + opendata.graficos.plazos.alto() / 2 + ')')
                                .text(parseInt(s).toLocaleString() + ' días')
                                .attr('font-size', '.9em')
                                .attr('text-anchor', 'middle');
                    } else {
                        alert('No existen modificaciones de plazos para este contrato');
                    }

                });
            } else {
                alert('No existe registro para el identificador de contrato proveido');
            }
        });
    }
};
opendata.graficos.gantt = {
    'ancho': function () {
        return parseInt(document.documentElement.clientWidth) || 0;
    },
    'alto': function () {
        return parseInt(document.documentElement.clientWidth / 4) || 0;
    },
    graficar: function (id_contrato) {
        $(opendata.graficos.globals.titulo_gantt).html('<h3 align="center"><label>Registro histórico del contrato y sus modificaciones</label><br><small>Cuándo se celebraron el contrato y sus modificaciones, y por cuánto tiempo respectivamente</small></h3>');
        $(opendata.graficos.globals.grafico_gantt).empty();
        /* Recuperamos los datos del contrato. Debemos obtener en la columna value el plazo correspondiente al contrato */
        d3.json('/contratos?id=' + id_contrato, function (contrato) {
            if (contrato.length) {
                d3.json('/modificaciones?id_contrato=' + id_contrato, function (modificaciones) {
                    function mouseover(p) {
                        opendata.graficos.utils.mouseover(p);
                    }
                    function mouseout() {
                        opendata.graficos.utils.mouseout();
                    }
                    function mousemove() {
                        opendata.graficos.utils.mousemove()
                    }
                    var dataSet = contrato.concat(modificaciones);
                    var dataInfo = {};
                    legendRectSize = 18;
                    legendSpacing = 4;
                    duration = 1000;
                    delay = duration / dataSet.length;

                    for (var datum in dataSet) {

                        if (_.has(dataSet, datum)) {
                            var uid = _.uniqueId('a');
                            // Add unique id
                            dataSet[datum]['uid'] = uid

                            // Create associate array
                            dataInfo[uid] = dataSet[datum];
                        }
                    }

                    var tipos = ['Contrato', 'Ampliación de plazo', 'Ampliación de monto'];

                    var colorTipoMap = {
                        'CONTRATO': '#1f77b4',
                        'AMP_PLAZO': 'orange',
                        'AMP_MONTO': 'green'
                    };
                    var descripcionToCodigoTipoModificacion = {
                        'Contrato': 'CONTRATO',
                        'Ampliación de plazo': 'AMP_PLAZO',
                        'Ampliación de monto': 'AMP_MONTO'
                    }

                    var DATA = {
                        dt: dataSet
                    }

                    var window_width = $(document).width();

                    // How many projects do you have?
                    var prjs = dataSet.length;

                    // Toggler
                    var toggler = true;

                    // Dimensions
                    var bar_height = 20;
                    var row_height = bar_height + 10;
                    var vertical_padding = 150
                    var bar_start_offset = 40
                    var w = window_width - 200;
                    var h = prjs * row_height + vertical_padding;

                    var svg = d3.select(opendata.graficos.globals.grafico_gantt)
                            .append('svg')
                            .attr('width', w)
                            .attr('height', h + 50);

                    function getDate(date) {
                        return new Date(date);
                    }
                    function accessStartDate(d) {
                        return getDate(d.startDate);
                    }
                    function accessEndDate(d) {
                        return getDate(d.endDate);
                    }

                    var min = d3.min(dataSet, accessStartDate);
                    var max = d3.max(dataSet, accessEndDate);

                    var paddingLeft = 300;
                    var paddingTop = 50;

                    var xScale = d3.time.scale()
                            .domain([min, max]).nice()
                            .range([paddingLeft, w]);

                    var locale = d3
                            .locale({
                                "decimal": ".",
                                "thousands": ",",
                                "grouping": [3],
                                "currency": ["$", ""],
                                "dateTime": "%a %b %e %X %Y",
                                "date": "%m/%d/%Y",
                                "time": "%H:%M:%S",
                                "periods": ["AM", "PM"],
                                "days": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                                "shortDays": ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
                                "months": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"],
                                "shortMonths": ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"]
                            });

                    var xAxis = d3
                            .svg.axis()
                            .scale(xScale)
                            .orient('bottom')
                            .tickFormat(locale.timeFormat("%b %Y"));

                    // Lines
                    var line = svg.append('g')
                            .selectAll('line')
                            .data(xScale.ticks(10))
                            .enter().append('line')
                            .attr('x1', xScale)
                            .attr('x2', xScale)
                            .attr('y1', paddingTop + 30)
                            .attr('y2', h - 50)
                            .style('stroke', '#ccc');

                    var y = function (i) {
                        return i * row_height + paddingTop + bar_start_offset;
                    }

                    labelY = function (i) {
                        return i * row_height + paddingTop + bar_start_offset + 13;
                    }

                    drawLegend(svg, h);

                    var bar = svg.selectAll('rect')
                            .data(DATA.dt, function (key) {
                                return key.uid
                            })
                            .enter()
                            .append('rect')
                            .attr('fill', '#fff')
                            .style('opacity', '0.9')
                            .on('mouseover', function (d) {
                                mouseover(d);
                            })
                            .on('mouseout', mouseout)
                            .on('mousemove', mousemove)
                            .transition()
                            .duration(1500)
                            .delay(function (d, i) {
                                return i * delay;
                            });
                    bar
                            .attr('y', function (d, i) {
                                return y(i);
                            })
                            .attr('x', function (d) {
                                return xScale(getDate(d.startDate))
                            })
                            .attr('width', function (d) {
                                return (xScale(getDate(d.endDate)) - xScale(getDate(d.startDate)));
                            })
                            .attr('height', bar_height)
                            .style('fill', function (d) {
                                return alternatingColorScale(d, d.uid);
                            })
                            .style('opacity', '1')
                            .attr('cursor', 'pointer')
                            .style('stroke', 'black')
                            .style('stroke-width', '0.5px');

                    var label = svg.selectAll('text')
                            .data(DATA.dt, function (key) {
                                return key.uid;
                            });

                    label.enter().append('text')
                            .attr('class', 'bar-label')
                            .attr('x', paddingLeft - 10)
                            .attr('y', function (d, i) {
                                return labelY(i);
                            })
                            .text(function (d) {
                                return d.id
                            });

                    // Bottom Axis
                    var btmAxis = svg.append('g')
                            .attr('transform', 'translate(0,' + (h - 25) + ')')
                            .attr('class', 'axis')
                            .call(xAxis);

                    // Top Axis
                    var topAxis = svg.append('g')
                            .attr('transform', 'translate(0,' + paddingTop + ')')
                            .attr('class', 'axis')
                            .call(xAxis);



                    function alternatingColorScale(d, codigo) {
                        if (codigo) {
                            return colorTipoMap[dataInfo[codigo]._tipo];
                        } else {
                            return colorTipoMap[descripcionToCodigoTipoModificacion[d]];
                        }
                    }

                    /* Leyenda de colores */
                    function drawLegend(chart, chartHeight) {
                        var hor = 50;
                        var ver = chartHeight + 30;
                        var legend = chart.selectAll('.legend').data(tipos).enter()
                                .append('g').attr('class', 'legend').attr('transform',
                                function (d, i) {
                                    hor = hor + opendata.graficos.gantt.ancho() / 5;
                                    return 'translate(' + hor + ',' + ver + ')';
                                });

                        legend.append('rect')
                                .attr('width', legendRectSize)
                                .attr('height', legendRectSize - 10)
                                .style('fill', function (d) {
                                    return alternatingColorScale(d);
                                })
                                .style('stroke', function (d) {
                                    return alternatingColorScale(d);
                                });

                        legend.append('text')
                                .attr('x', legendRectSize + legendSpacing)
                                .attr('y', legendRectSize - legendSpacing - 6)
                                .style('font', '12px sans-serif')
                                .text(function (d) {
                                    return d;
                                });
                    }
                });
            }
        });
    }
};
opendata.graficos.modificaciones = {
    'criterio_agrupamiento': '_tipo', /* Valor por defecto */
    'color': '_tipo', /* Valor por defecto */
    'radioMinimo': 10,
    'radioMaximo': 30,
    'padding': 5,
    'svg': {},
    'referencia': {},
    'nodos': {},
    'obtenerIdCoincidencias': function () {
        $(".js-data-example-ajax").select2({
            ajax: {
                url: "/coincidencias",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        id: params.term
                    };
                },
                processResults: function (data) {
                    var resultado = [];
                    for (var i in data) {
                        resultado = resultado.concat([{id: data[i], text: data[i]}]);
                    }
                    return {
                        results: resultado
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: formatoDeResultados,
            templateSelection: mostrarSeleccionado
        });

        function formatoDeResultados(id) {
            if (id.loading)
                return id.text;

            var markup = '<div class="clearfix">' +
                    '<div class="col-sm-12">' + id.text + '</div>' +
                    '</div>';

            return markup;
        }

        function mostrarSeleccionado(id) {
            return id.text;
        }
    },
    'maxRadio': function (nodos) {
        return d3.max(_.pluck(nodos, 'radio'))
    },
    'force': d3.layout.force(),
    'ancho': function () {
        return parseInt(document.documentElement.clientWidth) || 0;
    },
    'alto': function () {
        return parseInt(document.documentElement.clientWidth / 2) || 0;
        //return parseInt(document.documentElement.clientWidth / 4) || 0;
    },
    'crear_grafico': function (modificaciones) {
        /* Cambios en los agrupadores */
        $(opendata.graficos.globals.btn_agrupar).on('click', function (event) {
            categoria = $(event.target).attr('id');
            $("#agrupar_por > .btn").removeClass("active");
            $("#" + categoria).addClass("active");
            opendata.graficos.modificaciones.criterio_agrupamiento = categoria;
            dibujar(opendata.graficos.modificaciones.criterio_agrupamiento);
        });

        $(opendata.graficos.globals.btn_color).on('click', function (event) {
            color = $(event.target).attr('id');
            $("#color > .btn").removeClass("active");
            $("#" + color).addClass("active");
            opendata.graficos.modificaciones.color = $(event.target).attr('name');
            cambiarColor(opendata.graficos.modificaciones.color);
        });
        /*
         $(opendata.graficos.globals.btn_agrupar).change(function () {
         dibujar(this.value);
         });
         $(opendata.graficos.globals.btn_color).change(function () {
         opendata.graficos.modificaciones.color = this.value;
         cambiarColor(this.value);
         });
         */
        this.reset();
        opendata.graficos.modificaciones.referencia = d3.select(opendata.graficos.globals.grafico_modificaciones)
                .append('svg')
                .attr('width', this.ancho() * .2)
                .attr('height', this.alto())
                .style('float', 'right')
                .style('display', 'block')
                .style('margin', 'auto');

        opendata.graficos.modificaciones.svg = d3.select(opendata.graficos.globals.grafico_modificaciones)
                .append('svg')
                .attr('width', this.ancho() * .8)
                .attr('height', this.alto())
                .style('display', 'block')
                .style('margin', 'auto');

        modificaciones = getMapeo(modificaciones);
        var padding = 5;

        var maxRadius = {
            AMP_MONTO: d3.max(_.pluck(modificaciones, 'monto')),
            AMP_PLAZO: d3.max(_.pluck(modificaciones, 'vigencia_dias'))
        };
        var nodos = opendata.graficos.modificaciones.svg.selectAll('circle')
                .data(modificaciones)
                .enter().append('circle')
                .attr('class', 'node')
                .attr('cx', function (d) {
                    return d.x;
                })
                .attr('cy', function (d) {
                    return d.x;
                })
                .attr('r', function (d) {
                    return d.radio;
                })
                .attr('fill', function (d) {
                    return opendata.graficos.utils.criterioToColor["_tipo"][d._tipo];
                })
                .on('mouseover', function (d) {
                    opendata.graficos.utils.mouseover(d);
                })
                .on('mouseout', function (d) {
                    opendata.graficos.utils.mouseout();
                })
                .on('mousemove', function (d) {
                    opendata.graficos.utils.mousemove();
                })
                .on('click', function (d) {

                    $(opendata.graficos.globals.titulo_contrato).html("<strong>Contrato con:</strong>   " + d.razon_social + "<br><small><strong>Identificador: " + d.id_contrato + "</strong></small><br>" +
                            '<small><em>Vea las etapas de este contrato en este <a href="https://www.contrataciones.gov.py/datos/visualizaciones/etapas-licitacion?id_llamado="' + d.id_contrato.split('-')[0] + '">enlace.</a></em></small>');
                    var svgLeyenda = d3.select(opendata.graficos.globals.titulo_contrato).append('svg')
                            .attr('width', 700)
                            .attr('height', 40)
                            .style('display', 'block')
                            .style('margin', 'auto');

                    function graficarLeyenda(svgLeyenda) {
                        var tipos = [{leyenda: 'Contrato', color: '#1f77b4'}, {leyenda: 'Ampliación de plazo', color: 'orange'}, {leyenda: 'Ampliación de monto', color: 'green'}];
                        var hor = 75, legendRectSize = 18, legendSpacing = 4;
                        var ver = 30;
                        var legend = svgLeyenda.selectAll('.legend').data(tipos).enter()
                                .append('g').attr('class', 'legend').attr('transform',
                                function (d, i) {
                                    text = 'translate(' + hor + ',' + ver + ')';
                                    hor = hor + 200;
                                    return text;
                                });

                        legend.append('rect')
                                .attr('width', legendRectSize)
                                .attr('height', legendRectSize - 10)
                                .style('fill', function (d) {
                                    return d.color;
                                })
                                .style('stroke', function (d) {
                                    return d.color;
                                });

                        legend.append('text')
                                .attr('x', legendRectSize + legendSpacing)
                                .attr('y', legendRectSize - legendSpacing - 6)
                                .style('font', '12px sans-serif')
                                .text(function (d) {
                                    return d.leyenda;
                                });
                    }
                    ;
                    graficarLeyenda(svgLeyenda);
                    opendata.graficos.montos.graficar(d.id_contrato);
                    opendata.graficos.plazos.graficar(d.id_contrato);
                    opendata.graficos.gantt.graficar(d.id_contrato);
                });
        var force = d3.layout.force();
        cambiarColor(this.color);
//        this.criterio_agrupamiento = $(opendata.graficos.globals.btn_agrupar).val()
        dibujar(this.criterio_agrupamiento);

        /* Setea la ubicacion de cada modificacion */
        function getMapeo(nodos) {
            /* Setea la ubicacion de cada modificacion */
            /* Obtenemos el maximo valor del campo de entre las modificaciones */
            nodosPlazos = [];
            for (var i in nodos) {
                if (nodos[i]._tipo == 'AMP_PLAZO') {
                    nodosPlazos = nodosPlazos.concat([nodos[i]]);
                }
            }
            var maximoPlazos = d3.max(_.pluck(nodosPlazos, "vigencia_dias"));
            var minimoPlazos = d3.min(_.pluck(nodosPlazos, "vigencia_dias"));

            maximoPlazos = (!maximoPlazos) ? 10 : maximoPlazos;
            minimoPlazos = (!minimoPlazos) ? 10 : minimoPlazos;
            minimoPlazos = (minimoPlazos == maximoPlazos) ? parseInt(minimoPlazos / 10) : minimoPlazos;

            var radioPlazos = d3.scale.linear()
                    .range([opendata.graficos.modificaciones.radioMinimo, opendata.graficos.modificaciones.radioMaximo])
                    .domain([minimoPlazos, maximoPlazos]);
            nodosMontos = [];
            for (var i in nodos) {
                if (nodos[i]._tipo != 'AMP_PLAZO') {
                    nodosMontos = nodosMontos.concat([nodos[i]]);
                }
            }
            var maximoMontos = d3.max(_.pluck(nodosMontos, "monto"));
            var minimoMontos = d3.min(_.pluck(nodosMontos, "monto"));

            maximoMontos = (!maximoMontos) ? 10 : maximoMontos;
            minimoMontos = (!minimoMontos) ? 10 : minimoMontos;
            minimoMontos = (minimoMontos == maximoMontos) ? parseInt(minimoMontos / 10) : minimoMontos;

            var radioMontos = d3.scale.linear()
                    .range([opendata.graficos.modificaciones.radioMinimo, opendata.graficos.modificaciones.radioMaximo])
                    .domain([minimoMontos, maximoMontos]);

            /* Agregamos coordenadas de centro y radio a cada modificacion */
            for (var i in nodos) {
                /* Se calcula el radio que tendra esta modificacion en proporcionalmente al tamanio del maximo.
                 * El valor minimo sera 15 en caso de no tener especificado el nombre del campo */
                if (nodos[i]._tipo == 'AMP_PLAZO') {
                    nodos[i].radio = radioPlazos(nodos[i]["vigencia_dias"]);
                } else {
                    nodos[i].radio = radioMontos(nodos[i]["monto"]);
                }
                nodos[i].x = nodos[i].x ? nodos[i].x : Math.random() * opendata.graficos.modificaciones.ancho() * .8;
                nodos[i].y = nodos[i].y ? nodos[i].y : Math.random() * opendata.graficos.modificaciones.alto();
            }

            /* Variables a utilizar*/
            radioMedio = (opendata.graficos.modificaciones.radioMaximo + opendata.graficos.modificaciones.radioMinimo) * .5;
            diferenciaConMedio = opendata.graficos.modificaciones.radioMaximo - radioMedio;
            diferenciaConMinimo = opendata.graficos.modificaciones.radioMaximo - opendata.graficos.modificaciones.radioMinimo;
            ubicacionXCirculo = opendata.graficos.modificaciones.ancho() * .04;
            ubicacionYCirculo1 = opendata.graficos.modificaciones.alto() * .6;
            ubicacionYCirculo2 = opendata.graficos.modificaciones.alto() * .3;
            ubicacionXLeyendaTamaños = opendata.graficos.modificaciones.ancho() * .01;
            ubicacionYLeyendaTamaños = opendata.graficos.modificaciones.alto() * .12;
            ubicacionXLeyendaColores = opendata.graficos.modificaciones.ancho() * .01;
            ubicacionYLeyendaColores = opendata.graficos.modificaciones.alto() * .8;
            ubicacionYCirculoColores = opendata.graficos.modificaciones.alto() * .85;

            var leyendasMontos = [
                {valor: maximoMontos, etiqueta: " Gs."},
                {valor: (maximoMontos + minimoMontos) * .5, etiqueta: " Gs."},
                {valor: minimoMontos, etiqueta: " Gs."}
            ];
            circulosReferenciaMontos = [
                {x: ubicacionXCirculo, y: ubicacionYCirculo1, radio: opendata.graficos.modificaciones.radioMaximo, relleno: 'green'},
                {x: ubicacionXCirculo, y: ubicacionYCirculo1 + diferenciaConMedio, radio: radioMedio, relleno: 'green'},
                {x: ubicacionXCirculo, y: ubicacionYCirculo1 + diferenciaConMinimo, radio: opendata.graficos.modificaciones.radioMinimo, relleno: 'green'}
            ];
            var leyendasPlazos = [
                {valor: maximoPlazos, etiqueta: " Días"},
                {valor: (maximoPlazos + minimoPlazos) * .5, etiqueta: " Días"},
                {valor: minimoPlazos, etiqueta: " Días"}
            ];
            circulosReferenciaPlazos = [
                {x: ubicacionXCirculo, y: ubicacionYCirculo2, radio: opendata.graficos.modificaciones.radioMaximo, relleno: 'orange'},
                {x: ubicacionXCirculo, y: ubicacionYCirculo2 + diferenciaConMedio, radio: radioMedio, relleno: 'orange'},
                {x: ubicacionXCirculo, y: ubicacionYCirculo2 + diferenciaConMinimo, radio: opendata.graficos.modificaciones.radioMinimo, relleno: 'orange'}
            ];
            circulosReferenciaColores = [
                {x: ubicacionXCirculo * .7, y: ubicacionYCirculoColores, radio: opendata.graficos.modificaciones.radioMinimo, relleno: 'orange', texto: 'Ampl. de Plazos'},
                {x: ubicacionXCirculo * .7, y: ubicacionYCirculoColores * 1.1, radio: opendata.graficos.modificaciones.radioMinimo, relleno: 'green', texto: 'Ampl. de Montos'},
                {x: ubicacionXCirculo * 3, y: ubicacionYCirculoColores, radio: opendata.graficos.modificaciones.radioMinimo, relleno: 'red', texto: 'Guaranies'},
                {x: ubicacionXCirculo * 3, y: ubicacionYCirculoColores * 1.1, radio: opendata.graficos.modificaciones.radioMinimo, relleno: 'blue', texto: 'Dolares'}
            ];

            opendata.graficos.modificaciones.referencia
                    .selectAll('text')
                    .data(circulosReferenciaColores)
                    .enter()
                    .append('text')
                    .style('font-size', '.9em')
                    .text(function (d) {
                        return d.texto;
                    })
                    .attr('transform', function (d) {
                        return 'translate(' + (d.x + 15) + ',' + (d.y + 4) + ')'
                    });

            /* Agregamos lo referente a la referencia del panel derecho */


            lineas = [];
            for (var i in circulosReferenciaMontos) {
                lineas = lineas.concat([
                    [
                        {x: circulosReferenciaMontos[i].x + circulosReferenciaMontos[i].radio / 2, y: circulosReferenciaMontos[i].y - circulosReferenciaMontos[i].radio * .866},
                        {x: circulosReferenciaMontos[i].x + opendata.graficos.modificaciones.radioMaximo * 1.5, y: circulosReferenciaMontos[i].y - circulosReferenciaMontos[i].radio * .866}
                    ]
                ]);
                opendata.graficos.modificaciones.referencia
                        .append('text')
                        .style('font-size', '.9em')
                        //.attr('class', 'label')
                        .text(opendata.graficos.utils.separadorMiles(leyendasMontos[i].valor) + leyendasMontos[i].etiqueta)
                        .attr('transform', 'translate(' + (circulosReferenciaMontos[i].x + opendata.graficos.modificaciones.radioMaximo * 1.5 + 5) + ', ' + (circulosReferenciaMontos[i].y - circulosReferenciaMontos[i].radio * .866 + 3) + ')');
            }

            for (var i in circulosReferenciaPlazos) {
                lineas = lineas.concat([
                    [
                        {x: circulosReferenciaPlazos[i].x + circulosReferenciaPlazos[i].radio / 2, y: circulosReferenciaPlazos[i].y - circulosReferenciaPlazos[i].radio * .866},
                        {x: circulosReferenciaPlazos[i].x + opendata.graficos.modificaciones.radioMaximo * 1.5, y: circulosReferenciaPlazos[i].y - circulosReferenciaPlazos[i].radio * .866}
                    ]
                ]);
                opendata.graficos.modificaciones.referencia
                        .append('text')
                        .style('font-size', '.9em')
                        .text(opendata.graficos.utils.separadorMiles(leyendasPlazos[i].valor) + leyendasPlazos[i].etiqueta)
                        .attr('transform', 'translate(' + (circulosReferenciaPlazos[i].x + opendata.graficos.modificaciones.radioMaximo * 1.5 + 5) + ', ' + (circulosReferenciaPlazos[i].y - circulosReferenciaPlazos[i].radio * .866 + 3) + ')');
            }

            circulos = circulosReferenciaMontos.concat(circulosReferenciaPlazos).concat(circulosReferenciaColores);

            opendata.graficos.modificaciones.referencia
                    .selectAll('circle')
                    .data(circulos)
                    .enter().append('circle')
                    .attr('class', 'node')
                    .style('fill', function (d) {
                        return d.relleno;
                    })
                    .attr('cx', function (d) {
                        return d.x;
                    })
                    .attr('cy', function (d) {
                        return d.y;
                    })
                    .attr('r', function (d) {
                        return d.radio;
                    })
                    .attr('fill-opacity', function (d) {
                        return (d.relleno == 'gray') ? 0 : 1;
                    })
                    .attr('stroke-opacity', .5)
                    .attr('stroke-dasharray', function (d) {
                        return (d.relleno == 'gray') ? '5, 2' : '0';
                    });

            var line = d3.svg.line()
                    .x(function (d) {
                        return d.x;
                    })
                    .y(function (d) {
                        return d.y;
                    });

            opendata.graficos.modificaciones.referencia
                    .selectAll("path")
                    .data(lineas)
                    .enter()
                    .append("path")
                    .attr('d', line)
                    .attr('fill', 'none')
                    .attr('stroke-opacity', .5)
                    .attr('stroke', 'black')
                    .attr('stroke-width', 1);

            opendata.graficos.modificaciones.referencia
                    .append('text')
                    .style('font-size', '.9em')
                    .text("Tamaño de los círculos")
                    .attr('transform', 'translate(' + ubicacionXLeyendaTamaños + ',' + ubicacionYLeyendaTamaños + ')');

            opendata.graficos.modificaciones.referencia
                    .append('text')
                    .style('font-size', '.9em')
                    .text("Colores")
                    .attr('transform', 'translate(' + ubicacionXLeyendaColores + ',' + ubicacionYLeyendaColores + ')');

            opendata.graficos.modificaciones.referencia
                    .append('text')
                    .style('font-size', '1.2em')
                    .attr('class', 'label')
                    .text("Referencia")
                    .attr('opacity', '0.5')
                    .attr('transform', 'translate(' + (opendata.graficos.modificaciones.ancho() * .1 - 40) + ', 20)');

            return nodos;
        }
        /* Funcion para dibujar de acuerdo a la agrupacion */
        function dibujar(agruparPor) {
            var centros = getCentros(agruparPor, [opendata.graficos.modificaciones.ancho() * .8, opendata.graficos.modificaciones.alto()]);
            force.on('tick', marcar(centros, agruparPor));
            etiquetar_nodos(centros, agruparPor);
            force.start();
        }
        /* Funcion que obtiene los nombres para cada grupo */
        function getCentros(agruparPor, tamanio_lienzo) {
            /* La clave 'name' es para la etiqueta */
            var centros = _.uniq(_.pluck(modificaciones, agruparPor)).map(function (d) {
                if (agruparPor == "mes") {
                    return {name: d, value: (!d) ? 1 : d, directo: false};
                } else {
                    function hash(element) {
                        var hash = 0, i, chr, len;
                        if (element.length == 0)
                            return hash;
                        for (i = 0, len = element.length; i < len; i++) {
                            chr = element.charCodeAt(i);
                            hash = hash + chr;
                        }
                        return hash;
                    }
                    return {name: d, value: hash(d), directo: (agruparPor == '_tipo') ? false : true};
                }
            });
            function comparator(a, b) {
                return b.value - a.value;
            }
            /* Agrega los campos requeridos al conjunto anterior para el mapeo */
            d3.layout.treemap()
                    .size(tamanio_lienzo)
                    .ratio(1 / 1)
                    .nodes({children: centros})
                    .sort(comparator);
            return centros;
        }
        /* Funcion que hace interactuar a los circulos */
        function marcar(centros, criterio) {
            var grupos = {};
            for (i in centros) {
                grupos[centros[i].name] = centros[i];
            }

            /* Funcion utilizada en el retorno */
            function colisionar(alpha) {
                var quadtree = d3.geom.quadtree(modificaciones);
                return function (d) {
                    var r = d.radio + maxRadius[d._tipo] + padding,
                            nx1 = d.x - r,
                            nx2 = d.x + r,
                            ny1 = d.y - r,
                            ny2 = d.y + r;
                    quadtree.visit(function (quad, x1, y1, x2, y2) {
                        if (quad.point && (quad.point !== d)) {
                            var x = d.x - quad.point.x,
                                    y = d.y - quad.point.y,
                                    l = Math.sqrt(x * x + y * y),
                                    r = d.radio + quad.point.radio + padding;
                            if (l < r) {
                                l = (l - r) / l * alpha;
                                d.x -= x *= l;
                                d.y -= y *= l;
                                quad.point.x += x;
                                quad.point.y += y;
                            }
                        }
                        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                    });
                };
            }

            return function (e) {
                for (i in modificaciones) {
                    var o = modificaciones[i];
                    var f = grupos[o[criterio]];
                    o.y += ((f.y + (f.dy / 2)) - o.y) * e.alpha;
                    o.x += ((f.x + (f.dx / 2)) - o.x) * e.alpha;
                }
                nodos.each(colisionar(.11))
                        .attr('cx', function (d) {
                            return d.x;
                        })
                        .attr('cy', function (d) {
                            return d.y;
                        });
            }

        }
        /* Funcion para setear las etiquetas de grupo */
        function etiquetar_nodos(centros, agruparPor) {
            opendata.graficos.modificaciones.svg.selectAll('.label').remove();
            opendata.graficos.modificaciones.svg.selectAll('.label')
                    .data(centros)
                    .enter()
                    .append('text')
                    .style('font-size', function (d) {
                        if (d.directo) {
                            return '.7em';
                        }
                        return '1.2em';
                    })
                    .attr('class', 'label')
                    .text(function (d) {
                        if (d.directo) {
                            function abreviar(element) {
                                var res = "", palabras = element.split(" ");
                                for (var i in palabras) {
                                    palabras[i] = palabras[i].substr(0, 4);
                                    if (palabras[i].length == 4) {
                                        palabras[i] = palabras[i] + "."
                                    }
                                    res = res + palabras[i] + " ";
                                }
                                return res;
                            }
                            function recortar(element) {
                                if (element.length > 40) {
                                    element = element.substr(0, 39) + "...";
                                }
                                return element;
                            }

                            return recortar(d.name);
                            //return abreviar(d.name);
                        }
                        return opendata.graficos.utils.codigoToDescripcion[agruparPor][d.name];
                    })
                    .attr('transform', function (d) {
                        return 'translate(' + (d.x + (d.dx / 2) * .6) + ', ' + (d.y + 20) + ')';
                    });
        }
        function cambiarColor(criterio) {
            d3.selectAll('circle')
                    .transition()
                    .attr('fill', function (d) {
                        return opendata.graficos.utils.criterioToColor[criterio][d[criterio]];
                    })
                    .duration(1000);
        }
    },
    'graficar': function () {
        /* Limpiamos los graficos de montos, plazos y gantt */
        this.reset();
        /* Llamada ajax para graficar la primera vista */
        $.ajax({
            'type': 'POST',
            'url': '/modificaciones',
            'data': this.obtenerParametros(),
            'dataType': 'json',
            'success': function (datos) {
                opendata.graficos.modificaciones.crear_grafico(datos);
            }
        })
    },
    'reset': function () {
        $(opendata.graficos.globals.titulo_contrato).html("");
        $(opendata.graficos.globals.grafico_modificaciones).empty();
        $(opendata.graficos.globals.titulo_montos).empty();
        $(opendata.graficos.globals.grafico_montos).empty();
        $(opendata.graficos.globals.titulo_plazos).empty();
        $(opendata.graficos.globals.grafico_plazos).empty();
        $(opendata.graficos.globals.titulo_gantt).empty();
        $(opendata.graficos.globals.grafico_gantt).empty();
    },
    'obtenerParametros': function () {
        return JSON.stringify({
            'id_contrato': $(opendata.graficos.globals.select2_id_contrato).select2("val"),
            'categoria': $(opendata.graficos.globals.select2_categoria_contrato).select2("val"),
            'cantidad': $(opendata.graficos.globals.combobox_cantidad_modificaciones).val(),
            'anio': $(opendata.graficos.globals.select2_anio).select2("val"),
            'convocantes': $(opendata.graficos.globals.select2_convocante).select2("val")
        })
    }
};
opendata.graficos.iniciar = function () {
    $(".js-example-basic-multiple").select2({
        'placeholder': "-- Seleccione uno o mas valores --",
        'max-width': "50%"
    });

    opendata.graficos.utils.tooltip = d3.select('body').append('div')
            .style('position', 'absolute')
            .style('z-index', '10')
            .style('visibility', 'hidden')
            .style('color', 'white').style('padding', '8px')
            .style('background-color', 'rgba(0, 0, 0, 0.75)')
            .style('border-radius', '6px')
            .style('font', '12px sans-serif')
            .style('width', '400px')
            .text('tooltip');
    opendata.graficos.modificaciones.obtenerIdCoincidencias();
    opendata.graficos.modificaciones.graficar();

};
opendata.graficos.reset = function () {
    $(opendata.graficos.globals.select2_id_contrato).select2("val", "");

};