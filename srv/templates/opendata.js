function crearEspacioNombres(nombre){
    var nombres = nombre.split(".");
    var padre = window;
    for (var i in nombres){
        (!padre[nombres[i]])? padre[nombres[i]]={} : padre = padre[nombres[i]];
    }
}
crearEspacioNombres("opendata.graficos.globals");
crearEspacioNombres("opendata.graficos.utils");
crearEspacioNombres("opendata.graficos.modificaciones");
crearEspacioNombres("opendata.graficos.montos");
crearEspacioNombres("opendata.graficos.plazos");
crearEspacioNombres("opendata.graficos.gantt");