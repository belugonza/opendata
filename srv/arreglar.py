#!/usr/bin/python
# -*- encoding: utf-8 -*-

import pandas as pd
import numpy as np

df_modificaciones = pd.read_csv('csv/modificacion_contrato/completo.csv', encoding='utf-8', parse_dates=True)
df_modificaciones['monto'].fillna(0, inplace=True)
#df_modificaciones[['monto']] = df_modificaciones[['monto']].astype(int)

df_contratos = pd.read_csv('csv/contratos/completo.csv', encoding='utf-8', parse_dates=True)
df_contratos['_tipo'] = 'CONTRATO'

