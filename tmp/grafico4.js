/* Diagrama de GANTT para modificaciones de contrato */
function graficar_gant(id_contrato) {
    $(_titulo_gantt).html('<h3 align="center"><label>Diagrama de Gantt para las modificaciones</label></h3>');
    $(_grafico_gantt).empty();

    /* Recuperamos los datos del contrato. Debemos obtener en la columna value el plazo correspondiente al contrato */
    d3.json('/contratos?id=' + id_contrato + '&value=gantt' + '&top=' + $(_combobox_cantidad_modificaciones).val(), function (contrato) {
        if (contrato.length) {
            d3.json('/modificaciones?id_contrato=' + id_contrato + '&value=gantt' + '&top=' + $(_combobox_cantidad_modificaciones).val(), function (modificaciones) {

                var dataSet = contrato.concat(modificaciones);
                var dataInfo = {};
                legendRectSize = 18;
                legendSpacing = 4;
                duration = 1000;
                delay = duration / dataSet.length;

                // D3 requires each object to have a unique key for object constancy
                // http://bost.ocks.org/mike/constancy/
                // We can use underscore to create that if we're not sure if they exist in our data
                // Useful if you're updating data quickly (realtime, daily) and need otherwise meaningless uids

                // Also useful for making an associative array for hover info
                for (var datum in dataSet) {

                    if (_.has(dataSet, datum)) {
                        var uid = _.uniqueId('a');
                        // Add unique id
                        dataSet[datum]['uid'] = uid

                        // Create associate array
                        dataInfo[uid] = dataSet[datum];
                    }
                }
                var tooltip = d3.select('body').append('div')
                        .style('position', 'absolute')
                        .style('z-index', '10')
                        .style('visibility', 'hidden')
                        .style('color', 'white').style('padding', '8px')
                        .style('background-color', 'rgba(0, 0, 0, 0.75)')
                        .style('border-radius', '6px')
                        .style('font', '12px sans-serif')
                        .text('tooltip');

                var tipos = ['Contrato', 'Ampliación de plazo', 'Ampliación de monto',
                    'Reajuste', 'Renovación', 'Otros'];

                var colorTipoMap = {
                    'CONTRATO': '#4cc1e9',
                    'AMP_PLAZO': 'orange',
                    'AMP_MONTO': 'red',
                    'REAJUSTE': 'green',
                    'REN': 'yellow',
                    'OTROS': 'blue'
                };

                var DATA = {
                    dt: dataSet
                }

                var window_width = $(document).width();

                // How many projects do you have?
                var prjs = dataSet.length;

                // Toggler
                var toggler = true;

                // Dimensions
                var bar_height = 20;
                var row_height = bar_height + 10;
                var vertical_padding = 150
                var bar_start_offset = 40
                var w = window_width - 200;
                var h = prjs * row_height + vertical_padding;

                var svg = d3.select(_grafico_gantt)
                        .append('svg')
                        .attr('width', w)
                        .attr('height', h + 50);

                function getDate(date) {
                    return new Date(date);
                }
                function accessStartDate(d) {
                    return getDate(d.startDate);
                }
                function accessEndDate(d) {
                    return getDate(d.endDate);
                }

                var min = d3.min(dataSet, accessStartDate);
                var max = d3.max(dataSet, accessEndDate);

                var paddingLeft = 300;
                var paddingTop = 50;

                var xScale = d3.time.scale()
                        .domain([min, max]).nice()
                        .range([paddingLeft, w]);

                var xAxis = d3.svg.axis()
                        .scale(xScale)
                        .orient('bottom');

                // Lines
                var line = svg.append('g')
                        .selectAll('line')
                        .data(xScale.ticks(10))
                        .enter().append('line')
                        .attr('x1', xScale)
                        .attr('x2', xScale)
                        .attr('y1', paddingTop + 30)
                        .attr('y2', h - 50)
                        .style('stroke', '#ccc');

                var y = function (i) {
                    return i * row_height + paddingTop + bar_start_offset;
                }

                labelY = function (i) {
                    return i * row_height + paddingTop + bar_start_offset + 13;
                }

                drawLegend(svg, h);

                var bar = svg.selectAll('rect')
                        .data(DATA.dt, function (key) {
                            return key.uid
                        })
                        .enter()
                        .append('rect')
                        .attr('fill', '#fff')
                        .style('opacity', '0.9')
                        .on('mouseover', function (d) {
                            mouseover(d);
                        })
                        .on('mouseout', mouseout)
                        .on('mousemove', mousemove)
                        .transition()
                        .duration(1500)
                        .delay(function (d, i) {
                            return i * delay;
                        });
                bar
                        .attr('y', function (d, i) {
                            return y(i);
                        })
                        .attr('x', function (d) {
                            return xScale(getDate(d.startDate))
                        })
                        .attr('width', function (d) {
                            return (xScale(getDate(d.endDate)) - xScale(getDate(d.startDate)));
                        })
                        .attr('height', bar_height)
                        .style('fill', function (d) {
                            return alternatingColorScale(d, d.uid);
                        })
                        .style('opacity', '1')
                        .attr('cursor', 'pointer')
                        .style('stroke', 'black')
                        .style('stroke-width', '0.5px');

                var label = svg.selectAll('text')
                        .data(DATA.dt, function (key) {
                            return key.uid
                        });

                label.enter().append('text')
                        .attr('class', 'bar-label')
                        .attr('x', paddingLeft - 10)
                        .attr('y', function (d, i) {
                            return labelY(i);
                        })
                        .text(function (d) {
                            return d.id
                        });

                // Bottom Axis
                var btmAxis = svg.append('g')
                        .attr('transform', 'translate(0,' + (h - 25) + ')')
                        .attr('class', 'axis')
                        .call(xAxis);

                // Top Axis
                var topAxis = svg.append('g')
                        .attr('transform', 'translate(0,' + paddingTop + ')')
                        .attr('class', 'axis')
                        .call(xAxis);

                // Hover info
                function mouseover(d) {
                    tooltip.html(_mostrarTooltip(d, (d._tipo == 'CONTRATO') ? true : false, true))
                            .style('visibility', 'visible')
                            .style('stroke-width', '2px');
                }

                function mouseout() {
                    tooltip.style('visibility', 'hidden')
                            .style('stroke-width', '0.5px');
                }
                function mousemove() {
                    tooltip.style('top',
                            (d3.event.pageY - 10) + 'px').style('left', (d3.event.pageX + 10) + 'px');
                }

                function alternatingColorScale(d, codigo) {
                    if (codigo) {
                        return colorTipoMap[dataInfo[codigo]._tipo];
                    } else {
                        return colorTipoMap[tipoToCodigoModificacion[d]];
                    }

                }

                /* Leyenda de colores */
                function drawLegend(chart, chartHeight) {
                    var hor = 50;
                    var ver = chartHeight + 30;
                    var legend = chart.selectAll('.legend').data(tipos).enter()
                            .append('g').attr('class', 'legend').attr('transform',
                            function (d, i) {
                                hor = hor + 150;
                                return 'translate(' + hor + ',' + ver + ')';
                            });

                    legend.append('rect')
                            .attr('width', legendRectSize)
                            .attr('height', legendRectSize - 10)
                            .style('fill', function (d) {
                                return alternatingColorScale(d);
                            })
                            .style('stroke', function (d) {
                                return alternatingColorScale(d);
                            });

                    legend.append('text')
                            .attr('x', legendRectSize + legendSpacing)
                            .attr('y', legendRectSize - legendSpacing - 6)
                            .style('font', '12px sans-serif')
                            .text(function (d) {
                                return d;
                            });
                }

            });
        }
    });
}