/* Esto estaba en el original
 html, body {
 overflow:hidden;
 }
 */
var url_base = "http://servidor:5000";
var id_contrato = "186974-dc-ingenieria-s-a-1";
var id_contrato_monto = "255161-jorge-rolando-caceres-rojas-1";
//var top_registros = '';
var top_registros = '&top=50';

/* Valores para la escala de burbujas con montos en dolares */
var anioCambio = {
    "2010": 4741,
    "2011": 4190,
    "2012": 4425,
    "2013": 4311,
    "2014": 4477,
    "2015": 4784
};

/* Referencias a los div's */
var _combobox_anio = '#anio';
var _combobox_agrupar = '#agrupar_por'
var _combobox_tamanio_circulo = '#tamanio_circulo';
var _combobox_color = '#color';
var _grafico_modificaciones = '#chart1';
var _grafico_plazos = '#chart2';
var _grafico_gantt = '#chart3';
var _grafico_montos = '#chart4';

function _mostrarTooltip(nodo, esContrato, esGantt) {
    if (!esContrato) {
        if (esGantt) {
            return  "<strong>Identificador: </strong> " + nodo.id + "<br>" +
                    "<strong>Tipo de Modificacion: </strong> " + nodo.tipo + "<br>" +
                    "<strong>Fecha firma de Modificacion: </strong> " + moment(nodo.fecha_contrato.split(" ")[0]).format('DD-MM-YYYY') + "<br>" +
                    "<strong>Vigencia: </strong> " + nodo.vigencia_dias + " dias<br>" +
                    "<strong>Fecha inicio: </strong> " + moment(nodo.startDate.split(" ")[0]).format('DD-MM-YYYY') + "<br>" +
                    "<strong>Fecha fin: </strong> " + moment(nodo.endDate.split(" ")[0]).format('DD-MM-YYYY') + "<br>" +
                    "<strong>Monto: </strong> " + parseInt(nodo.monto).toLocaleString() + " " + nodo.moneda;
        } else {
            return  "<strong>Identificador: </strong> " + nodo.id + "<br>" +
                    "<strong>Tipo de Modificacion: </strong> " + nodo.tipo + "<br>" +
                    "<strong>Fecha firma de Modificacion: </strong> " + moment(nodo.fecha_contrato.split(" ")[0]).format('DD-MM-YYYY') + "<br>" +
                    "<strong>Vigencia: </strong> " + nodo.vigencia_dias + " dias<br>" +
                    "<strong>Monto: </strong> " + parseInt(nodo.monto).toLocaleString() + " " + nodo.moneda;
        }
    } else {
        if (esGantt) {
            return  "<strong>Identificador: </strong> " + nodo.id + "<br>" +
                    "<strong>Proveedor: </strong> " + nodo.razon_social + "<br>" +
                    "<strong>Llamado: </strong> " + nodo.nombre_licitacion + "<br>" +
                    "<strong>Tipo de procedimiento: </strong> " + nodo.tipo_procedimiento + "<br>" +
                    "<strong>Fecha firma contrato: </strong> " + moment(nodo.fecha_firma_contrato.split(" ")[0]).format('DD-MM-YYYY') + "<br>" +
                    "<strong>Fecha inicio: </strong> " + moment(nodo.startDate.split(" ")[0]).format('DD-MM-YYYY') + "<br>" +
                    "<strong>Fecha fin: </strong> " + moment(nodo.endDate.split(" ")[0]).format('DD-MM-YYYY') + "<br>" +
                    "<strong>Monto total adjudicado: </strong> " + parseInt(nodo.monto_adjudicado).toLocaleString() + " " + nodo.moneda;
        } else {
            return  "<strong>Identificador: </strong> " + nodo.id + "<br>" +
                    "<strong>Proveedor: </strong> " + nodo.razon_social + "<br>" +
                    "<strong>Llamado: </strong> " + nodo.nombre_licitacion + "<br>" +
                    "<strong>Tipo de procedimiento: </strong> " + nodo.tipo_procedimiento + "<br>" +
                    "<strong>Fecha firma contrato: </strong> " + moment(nodo.fecha_firma_contrato.split(" ")[0]).format('DD-MM-YYYY') + "<br>" +
                    "<strong>Monto total adjudicado: </strong> " + parseInt(nodo.monto_adjudicado).toLocaleString() + " " + nodo.moneda;
        }
    }
}

/* Grafica de modificaicones de contrato por anio */
function graficar_modificaciones_anio(anio) {

    var agrupamiento = tamanio_circulo = color = '';
    start(anio);

    function start(anio) {
        $.ajax({
            'url': url_base + '/modificaciones?anio=' + anio + top_registros,
            'dataType': 'json',
            'success': function (modificaciones) {
                $(_grafico_modificaciones).empty();
                crear_grafico(modificaciones);
            }
        });
    }

    function crear_grafico(modificaciones) {
        /* Setea la ubicacion de cada modificacion */
        function getMapeoDeModificaciones(modificaciones, nombre_del_campo) {
            /* Obtenemos el maximo valor del campo de entre las modificaciones */
            var max = d3.max(_.pluck(modificaciones, nombre_del_campo));
            if (!max) {
                max = 1;
            }
            /* Agregamos coordenadas de centro y radio a cada modificaion */
            for (var i in modificaciones) {
                /* Se calcula el radio que tendra esta modificacion en proporcionalmente al tamanio del maximo.
                 * El valor minimo sera 15 en caso de no tener especificado el nombre del campo */
                modificaciones[i].radius = (nombre_del_campo != '') ? radius * (modificaciones[i][nombre_del_campo] / max) : 15;
                modificaciones[i].x = modificaciones[i].x ? modificaciones[i].x : Math.random() * width;
                modificaciones[i].y = modificaciones[i].y ? modificaciones[i].y : Math.random() * height;
            }

            return modificaciones;
        }

        /* Categoriza al monto de la modificacion en ALTO, MEDIO, BAJO */
        function getCategoriaDeMonto(modificacion) {
            var max = d3.max(_.pluck(modificaciones, 'monto'));
            if (!max) {
                max = 1;
            }
            var val = modificacion['monto'] / max;

            if (val > 0.4)
                return 'Mayores';
            else if (val > 0.1)
                return 'Medios';
            else
                return 'Bajos';
        }

        /* Funcion para dibujar de acuerdo a la agrupacion */
        function dibujar(criterio) {
            var centros = getCentros(criterio, [width, height]);
            force.on("tick", marcar(centros, criterio));
            etiquetar_nodos(centros);
            force.start();
        }

        /* Funcion que obtiene los centros para las modificaciones */
        function getCentros(criterio, tamanio_lienzo) {
            /* La clave "name" es para la etiqueta */
            var centros = _.uniq(_.pluck(modificaciones, criterio)).map(function (d) {
                return {name: d, value: 1};
            });

            /* NO SE PARA QUE SE HACE ESTO */
            var map = d3.layout.treemap()
                    .size(tamanio_lienzo)
                    .ratio(1 / 1)
                    .nodes({children: centros});

            return centros;
        }

        /* No se exactamente que hace */
        function marcar(centros, criterio) {
            var focos = {};
            for (i in centros) {
                focos[centros[i].name] = centros[i];
            }

            /* Funcion utilizada en el retorno */
            function colisionar(alpha) {
                var quadtree = d3.geom.quadtree(modificaciones);
                return function (d) {
                    var r = d.radius + maxRadius + padding,
                            nx1 = d.x - r,
                            nx2 = d.x + r,
                            ny1 = d.y - r,
                            ny2 = d.y + r;
                    quadtree.visit(function (quad, x1, y1, x2, y2) {
                        if (quad.point && (quad.point !== d)) {
                            var x = d.x - quad.point.x,
                                    y = d.y - quad.point.y,
                                    l = Math.sqrt(x * x + y * y),
                                    r = d.radius + quad.point.radius + padding;
                            if (l < r) {
                                l = (l - r) / l * alpha;
                                d.x -= x *= l;
                                d.y -= y *= l;
                                quad.point.x += x;
                                quad.point.y += y;
                            }
                        }
                        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                    });
                };
            }

            return function (e) {
                for (i in modificaciones) {
                    var o = modificaciones[i];
                    var f = focos[o[criterio]];
                    o.y += ((f.y + (f.dy / 2)) - o.y) * e.alpha;
                    o.x += ((f.x + (f.dx / 2)) - o.x) * e.alpha;
                }
                nodos.each(colisionar(.11))
                        .attr("cx", function (d) {
                            return d.x;
                        })
                        .attr("cy", function (d) {
                            return d.y;
                        });
            }

        }

        /* Funcion para setear las etiquetas */
        function etiquetar_nodos(centros) {
            svg.selectAll(".label").remove();

            svg.selectAll(".label")
                    .data(centros)
                    .enter()
                    .append("text")
                    .attr("class", "label")
                    .text(function (d) {
                        return d.name;
                    })
                    .attr("transform", function (d) {
                        return "translate(" + (d.x + (d.dx / 2)) + ", " + (d.y + 20) + ")";
                    });
        }

        function cambiarColor(criterio) {
            d3.selectAll("circle")
                    .transition()
                    .style('fill', function (d) {
                        return criterio ? colors[criterio][d[criterio]] : colors['default']
                    })
                    .duration(1000);
            $('.colors').empty();
            if (criterio) {
                for (var i in colors[criterio]) {
                    $('.colors').append('<div class="col-xs-1 color-legend" style="background:' + colors[criterio][i] + ';">' + i + '</div>')
                }
            }
        }

        /* Funcion para remover el tooltip */
        function removerTooltip() {
            $('.popover').each(function () {
                $(this).remove();
            });
        }

        function mostrarTooltip(modificacion) {
            $(this).popover({
                placement: 'auto top',
                container: 'body',
                trigger: 'manual',
                html: true,
                content: function () {
                    return _mostrarTooltip(modificacion, false, false);
                }
            });
            $(this).popover('show')
        }

        /* Seccion de comportamiento de cambios de comboboxes */
        $(_combobox_anio).change(function () {
            start(this.value);
        });
        $(_combobox_agrupar).change(function () {
            agrupamiento = this.value;
            dibujar(agrupamiento);
        });
        $(_combobox_tamanio_circulo).change(function () {
            var criterio = this.value;
            var max = d3.max(_.pluck(modificaciones, criterio));
            if (!max) {
                max = 1;
            }
            d3.selectAll("circle")
                    .data(getMapeoDeModificaciones(modificaciones, this.value))
                    .transition()
                    .attr('r', function (d, i) {
                        return criterio ? (radius * (modificaciones[i][criterio] / max)) : 15;
                    })
                    .attr('cx', function (d) {
                        return d.x;
                    })
                    .attr('cy', function (d) {
                        return d.y;
                    })
                    .duration(1000);

            tamanio_circulo = this.value;

            force.start();
        });
        $(_combobox_color).change(function () {
            color = this.value;
            cambiarColor(color);
        });

        var colors = {
            _moneda: {
                USD: 'chartreuse',
                PYG: 'crimson'
            },
            _tipo: {
                AMP_MONTO: 'red',
                AMP_PLAZO: 'orange',
                OTROS: 'blue',
                REAJUSTE: 'green',
                REN: 'yellow'
            },
            default: '#4CC1E9'
        };
        /* El tamanio del radio del maximo valor encontrado */
        var radius = 75;
        //var width = parseInt(document.documentElement.clientWidth) || 0;
        //var height = parseInt(document.documentElement.clientWidth / 4) || 0;
        var width = 1000;
        var height = 400;

        var fill = d3.scale.linear()
                .range(['blue', 'green'])
                .domain([0, 5000]);
        var svg = d3.select(_grafico_modificaciones)
                .append("svg")
                .attr("width", width)
                .attr("height", height)
                .style('display', 'block')
                .style('margin', 'auto');
        modificaciones = getMapeoDeModificaciones(modificaciones, tamanio_circulo);
        var padding = 5;
        var maxRadius = d3.max(_.pluck(modificaciones, 'radius'));
        var maximums = {
            monto: d3.max(_.pluck(modificaciones, 'monto')),
            vigencia_dias: d3.max(_.pluck(modificaciones, 'vigencia_dias'))
        };
        var nodos = svg.selectAll("circle")
                .data(modificaciones)
                .enter().append("circle")
                .attr("class", "node")
                .attr("cx", function (d) {
                    return d.x;
                })
                .attr("cy", function (d) {
                    return d.x;
                })
                .attr("r", function (d) {
                    return d.radius;
                })
                .style("fill", function (d, i) {
                    return colors['default'];
                })
                .on("mouseover", function (d) {
                    mostrarTooltip.call(this, d);
                })
                .on("mouseout", function (d) {
                    removerTooltip();
                });

        var force = d3.layout.force();
        cambiarColor(color);
        dibujar(agrupamiento);
    }
}

graficar_modificaciones_anio(2010);