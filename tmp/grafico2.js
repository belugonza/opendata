/* Grafica de modificaciones de contrato por montos */
function graficar_modificacion_monto(id_contrato) {
    $(_titulo_montos).html('<h3 align="center"><label>Proporción por montos</label></h3>');
    $(_grafico_montos).empty();
    /* Recuperamos los datos del contrato. Debemos obtener en la columna value el plazo correspondiente al contrato */
    d3.json('/contratos?id=' + id_contrato + '&value=monto' + '&top=' + $(_combobox_cantidad_modificaciones).val(), function (contrato) {

        if (contrato.length) {

            d3.json('/modificaciones?id_contrato=' + id_contrato + '&value=monto' + '&top=' + $(_combobox_cantidad_modificaciones).val(), function (modificaciones) {
                /* Si se trajo algun registro */
                if (modificaciones.length) {
                    var width = parseInt(window.innerWidth / 4) || 0;
                    var height = parseInt(window.innerWidth / 4) || 0;

                    var r = parseInt(width * 2 / 5);
                    contrato[0].value = parseInt(contrato[0].value);

                    if (contrato[0]._moneda == 'PYG') {
                        s = contrato[0].value;
                    } else {
                        s = contrato[0].value * anioCambio[contrato.anio];
                        contrato[0].value = s;
                    }

                    for (var i in modificaciones) {
                        modificaciones[i].value = parseInt(modificaciones[i].value);
                        if (modificaciones[i]._moneda != 'PYG') {
                            modificaciones[i].value = modificaciones[i].value * anioCambio[contrato.anio];
                        }
                        s = s + modificaciones[i].value;
                    }
                    ;

                    var data_para_graficar = contrato.concat(modificaciones);

                     var color = d3.scale.category10();

                    var canvas = d3.select(_grafico_montos)
                            .append('svg')
                            .attr('width', width)
                            .attr('height', height)
                            .style('display', 'block')
                            .style('margin', 'auto');

                    var tooltip = d3.select('body').append('div')
                            .style('position', 'absolute')
                            .style('z-index', '10')
                            .style('visibility', 'hidden')
                            .style('color', 'white').style('padding', '8px')
                            .style('background-color', 'rgba(0, 0, 0, 0.75)')
                            .style('border-radius', '6px')
                            .style('font', '12px sans-serif')
                            .text('tooltip');

                    var group = canvas.append('g')
                            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')

                    var arc = d3.svg.arc()
                            .innerRadius(parseInt(r / 2))
                            .outerRadius(r);

                    var pie = d3.layout.pie()
                            .value(function (d) {
                                return d.value;
                            });

                    var arcs = group.selectAll('.arc')
                            .data(pie(data_para_graficar))
                            .enter()
                            .append('g')
                            .attr('class', 'arc');

                    arcs.append('path')
                            .attr('d', arc)
                            .attr('fill', function (d) {
                                return color(d.data.vigencia_dias)
                            });

                    arcs.append('text')
                            .attr('transform', function (d) {
                                return 'translate(' + arc.centroid(d) + ')'
                            })
                            .attr('text-anchor', 'middle')
                            .attr('font-size', '1.5em')
                            .text(function (d) {
                                if (d.data.value) {
                                    return d.data.value;
                                }
                            });

                    arcs.style('cursor', 'pointer')
                            .style('stroke', 'black')
                            .style('stroke-width', 0.5)
                            .on('mouseover', mouseover)
                            .on('mouseout', mouseout)
                            .on('mousemove', mousemove);

                    canvas.append('text')
                            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')
                            .text(s + ' Gs.')
                            .attr('font-size', '1.5em')
                            .attr('text-anchor', 'middle');

                    function mouseover(p) {
                        tooltip.html(_mostrarTooltip(p.data,(p.startAngle == 0)? true:false,false))
                                .style('visibility', 'visible');
                        d3.select(this).style('stroke-width', '2px');
                    }

                    function mouseout(p) {
                        tooltip.style('visibility', 'hidden');
                        d3.select(this).style('stroke-width', '0.5px');
                    }

                    function mousemove(p) {
                        tooltip.style('top',
                                (d3.event.pageY - 10) + 'px').style('left', (d3.event.pageX + 10) + 'px');
                    }

                } else {
                    alert('No existen modificaciones de montos para este contrato');
                }

            });
        } else {
            alert('No existe registro para el identificador de contrato proveido');
        }
    });
}