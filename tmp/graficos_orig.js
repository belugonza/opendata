/* Esto estaba en el original
 html, body {
 overflow:hidden;
 }
 */
var id_contrato = '186974-dc-ingenieria-s-a-1';
var id_contrato_monto = '255161-jorge-rolando-caceres-rojas-1';
//var top_registros = '';
var top_registros = '&top=50';

graficar_modificaciones_anio(2010);

/* Valores para la escala de burbujas con montos en dolares */
var anioCambio = {
    '2010': 4741,
    '2011': 4190,
    '2012': 4425,
    '2013': 4311,
    '2014': 4477,
    '2015': 4784
};

var categoria = {
    '0': 'Adquisición y Locación de inmuebles. Alquiler de muebles',
    '1': 'Bienes e insumos agropecuario y forestal',
    '2': 'Capacitaciones y Adiestramientos',
    '3': 'Combustibles y Lubricantes',
    '4': 'Construcción, Restauración, Reconstrucción o Remodelación y Reparación de Inmuebles',
    '5': 'Consultorías, Asesorías e Investigaciones. Estudios y Proyectos de inversión',
    '6': 'Elementos e insumos de limpieza',
    '7': 'Equipos Militares y de Seguridad. Servicio de Seguridad y Vigilancia',
    '8': 'Equipos, accesorios y programas computacionales, de oficina, educativos, de imprenta, de comunicación y señalamiento',
    '9': 'Equipos, Productos e instrumentales Médicos y de Laboratorio. Servicios asistenciales de salud',
    '10': 'Maquinarias, Equipos y herramientas mayores - Equipos de transporte',
    '11': 'Materiales e insumos eléctricos, metálicos y no metálicos, Plásticos, cauchos. Repuestos, herramientas, cámaras y cubiertas.',
    '12': 'Minerales',
    '13': 'Muebles y Enseres',
    '14': 'Pasajes y Transportes',
    '15': 'Productos Alimenticios',
    '16': 'Productos quimicos',
    '17': 'Publicidad y Propaganda',
    '18': 'Seguros',
    '19': 'Servicios de ceremonial, gastronomico y funerarios',
    '20': 'Servicios de Limpiezas, Mantenimientos y reparaciones menores y mayores de Instalaciones, Maquinarias y Vehículos',
    '21': 'Servicios Técnicos',
    '22': 'Textiles, vestuarios y calzados',
    '23': 'Utensilios de cocina y comedor, Productos de Porcelana, vidrio y loza',
    '24': 'Utiles de oficina, Productos de papel y cartón e impresos'
};

/* Referencias a los div's */
var _textbox_id_contrato = '#id_contrato';
var _combobox_anio = '#anio';
var _combobox_agrupar = '#agrupar_por'
var _combobox_tamanio_circulo = '#tamanio_circulo';
var _combobox_color = '#color';
var _grafico_modificaciones = '#chart1';
var _grafico_plazos = '#chart2';
var _grafico_gantt = '#chart3';
var _grafico_montos = '#chart4';

function _mostrarTooltip(nodo, esContrato, esGantt) {
    var moneda;
    if (nodo._moneda == 'USD') {
        moneda = nodo._moneda;
    } else {
        moneda = 'Gs.';
    }
    if (!esContrato) {
        if (esGantt) {
            return  '<strong>Identificador: </strong> ' + nodo.id + '<br>' +
                    '<strong>Tipo de Modificacion: </strong> ' + nodo.tipo + '<br>' +
                    '<strong>Fecha firma de Modificacion: </strong> ' + moment(nodo.fecha_contrato.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Vigencia: </strong> ' + nodo.vigencia_dias + ' dias<br>' +
                    '<strong>Fecha inicio: </strong> ' + moment(nodo.startDate.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Fecha fin: </strong> ' + moment(nodo.endDate.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Monto: </strong> ' + parseInt(nodo.monto).toLocaleString() + ' ' + moneda;
        } else {
            return  '<strong>Identificador: </strong> ' + nodo.id + '<br>' +
                    '<strong>Tipo de Modificacion: </strong> ' + nodo.tipo + '<br>' +
                    '<strong>Fecha firma de Modificacion: </strong> ' + moment(nodo.fecha_contrato.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Vigencia: </strong> ' + nodo.vigencia_dias + ' dias<br>' +
                    '<strong>Monto: </strong> ' + parseInt(nodo.monto).toLocaleString() + ' ' + moneda;
        }
    } else {
        if (esGantt) {
            return  '<strong>Identificador: </strong> ' + nodo.id + '<br>' +
                    '<strong>Proveedor: </strong> ' + nodo.razon_social + '<br>' +
                    '<strong>Llamado: </strong> ' + nodo.nombre_licitacion + '<br>' +
                    '<strong>Convocante:  </strong> ' + nodo.convocante + '<br>' +
                    '<strong>Categoria:  </strong> ' + nodo.categoria + '<br>' +
                    '<strong>Tipo de procedimiento: </strong> ' + nodo.tipo_procedimiento + '<br>' +
                    '<strong>Fecha firma contrato: </strong> ' + moment(nodo.fecha_firma_contrato.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Fecha inicio: </strong> ' + moment(nodo.startDate.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Fecha fin: </strong> ' + moment(nodo.endDate.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Monto total adjudicado: </strong> ' + parseInt(nodo.monto_adjudicado).toLocaleString() + ' ' + moneda;
        } else {
            return  '<strong>Identificador: </strong> ' + nodo.id + '<br>' +
                    '<strong>Proveedor: </strong> ' + nodo.razon_social + '<br>' +
                    '<strong>Llamado: </strong> ' + nodo.nombre_licitacion + '<br>' +
                    '<strong>Convocante:  </strong> ' + nodo.convocante + '<br>' +
                    '<strong>Categoria:  </strong> ' + nodo.categoria + '<br>' +
                    '<strong>Tipo de procedimiento: </strong> ' + nodo.tipo_procedimiento + '<br>' +
                    '<strong>Fecha firma contrato: </strong> ' + moment(nodo.fecha_firma_contrato.split(' ')[0]).format('DD-MM-YYYY') + '<br>' +
                    '<strong>Monto total adjudicado: </strong> ' + parseInt(nodo.monto_adjudicado).toLocaleString() + ' ' + moneda;
        }
    }
}

/* Grafica de modificaicones de contrato por anio */
function graficar_modificaciones_anio(anio) {

    var agrupamiento = tamanio_circulo = color = '';
    start(anio);

    function start(anio) {
        console.log();
        $.ajax({
            'url': '/modificaciones?anio=' + anio + '&id_contrato=' + $('#id_contrato').val() + top_registros,
            'dataType': 'json',
            'success': function (modificaciones) {
                $(_grafico_modificaciones).empty();
                $(_grafico_plazos).empty();
                $(_grafico_montos).empty();
                crear_grafico(modificaciones);
            }
        });
    }

    function crear_grafico(modificaciones) {
        /* Setea la ubicacion de cada modificacion */
        function getMapeoDeModificaciones(modificaciones, nombre_del_campo) {
            /* Obtenemos el maximo valor del campo de entre las modificaciones */
            var max = d3.max(_.pluck(modificaciones, nombre_del_campo));
            if (!max) {
                max = 1;
            }
            /* Agregamos coordenadas de centro y radio a cada modificaion */
            for (var i in modificaciones) {
                /* Se calcula el radio que tendra esta modificacion en proporcionalmente al tamanio del maximo.
                 * El valor minimo sera 15 en caso de no tener especificado el nombre del campo */
                modificaciones[i].radius = (nombre_del_campo != '') ? radius * (modificaciones[i][nombre_del_campo] / max) : 15;
                modificaciones[i].x = modificaciones[i].x ? modificaciones[i].x : Math.random() * width;
                modificaciones[i].y = modificaciones[i].y ? modificaciones[i].y : Math.random() * height;
            }

            return modificaciones;
        }

        /* Categoriza al monto de la modificacion en ALTO, MEDIO, BAJO */
        function getCategoriaDeMonto(modificacion) {
            var max = d3.max(_.pluck(modificaciones, 'monto'));
            if (!max) {
                max = 1;
            }
            var val = modificacion['monto'] / max;

            if (val > 0.4)
                return 'Mayores';
            else if (val > 0.1)
                return 'Medios';
            else
                return 'Bajos';
        }

        /* Funcion para dibujar de acuerdo a la agrupacion */
        function dibujar(criterio) {
            var centros = getCentros(criterio, [width, height]);
            force.on('tick', marcar(centros, criterio));
            etiquetar_nodos(centros);
            force.start();
        }

        /* Funcion que obtiene los centros para las modificaciones */
        function getCentros(criterio, tamanio_lienzo) {
            /* La clave 'name' es para la etiqueta */
            var centros = _.uniq(_.pluck(modificaciones, criterio)).map(function (d) {
                return {name: d, value: 1};
            });

            /* NO SE PARA QUE SE HACE ESTO */
            var map = d3.layout.treemap()
                    .size(tamanio_lienzo)
                    .ratio(1 / 1)
                    .nodes({children: centros});

            return centros;
        }

        /* No se exactamente que hace */
        function marcar(centros, criterio) {
            var focos = {};
            for (i in centros) {
                focos[centros[i].name] = centros[i];
            }

            /* Funcion utilizada en el retorno */
            function colisionar(alpha) {
                var quadtree = d3.geom.quadtree(modificaciones);
                return function (d) {
                    var r = d.radius + maxRadius + padding,
                            nx1 = d.x - r,
                            nx2 = d.x + r,
                            ny1 = d.y - r,
                            ny2 = d.y + r;
                    quadtree.visit(function (quad, x1, y1, x2, y2) {
                        if (quad.point && (quad.point !== d)) {
                            var x = d.x - quad.point.x,
                                    y = d.y - quad.point.y,
                                    l = Math.sqrt(x * x + y * y),
                                    r = d.radius + quad.point.radius + padding;
                            if (l < r) {
                                l = (l - r) / l * alpha;
                                d.x -= x *= l;
                                d.y -= y *= l;
                                quad.point.x += x;
                                quad.point.y += y;
                            }
                        }
                        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                    });
                };
            }

            return function (e) {
                for (i in modificaciones) {
                    var o = modificaciones[i];
                    var f = focos[o[criterio]];
                    o.y += ((f.y + (f.dy / 2)) - o.y) * e.alpha;
                    o.x += ((f.x + (f.dx / 2)) - o.x) * e.alpha;
                }
                nodos.each(colisionar(.11))
                        .attr('cx', function (d) {
                            return d.x;
                        })
                        .attr('cy', function (d) {
                            return d.y;
                        });
            }

        }

        /* Funcion para setear las etiquetas */
        function etiquetar_nodos(centros) {
            svg.selectAll('.label').remove();

            svg.selectAll('.label')
                    .data(centros)
                    .enter()
                    .append('text')
                    .attr('class', 'label')
                    .text(function (d) {
                        return d.name;
                    })
                    .attr('transform', function (d) {
                        return 'translate(' + (d.x + (d.dx / 2)) + ', ' + (d.y + 20) + ')';
                    });
        }

        function cambiarColor(criterio) {
            d3.selectAll('circle')
                    .transition()
                    .style('fill', function (d) {
                        return criterio ? colors[criterio][d[criterio]] : colors['default']
                    })
                    .duration(1000);
            $('.colors').empty();
            if (criterio) {
                for (var i in colors[criterio]) {
                    $('.colors').append('<div class="col-xs-1 color-legend" style="background:' + colors[criterio][i] + ';">' + i + '</div>')
                }
            }
        }

        /* Funcion para remover el tooltip */
        function removerTooltip() {
            $('.popover').each(function () {
                $(this).remove();
            });
        }

        function mostrarTooltip(modificacion) {
            $(this).popover({
                placement: 'auto top',
                container: 'body',
                trigger: 'manual',
                html: true,
                content: function () {
                    return _mostrarTooltip(modificacion, false, false);
                }
            });
            $(this).popover('show')
        }
        $(_textbox_id_contrato).change(function () {
            start($(_combobox_anio).val());
        });
        /* Seccion de comportamiento de cambios de comboboxes */
        $(_combobox_anio).change(function () {
            start(this.value);
        });
        $(_combobox_agrupar).change(function () {
            agrupamiento = this.value;
            dibujar(agrupamiento);
        });
        $(_combobox_tamanio_circulo).change(function () {
            var criterio = this.value;
            var max = d3.max(_.pluck(modificaciones, criterio));
            if (!max) {
                max = 1;
            }
            d3.selectAll('circle')
                    .data(getMapeoDeModificaciones(modificaciones, this.value))
                    .transition()
                    .attr('r', function (d, i) {
                        return criterio ? (radius * (modificaciones[i][criterio] / max)) : 15;
                    })
                    .attr('cx', function (d) {
                        return d.x;
                    })
                    .attr('cy', function (d) {
                        return d.y;
                    })
                    .duration(1000);

            tamanio_circulo = this.value;

            force.start();
        });
        $(_combobox_color).change(function () {
            color = this.value;
            cambiarColor(color);
        });

        var colors = {
            _moneda: {
                USD: 'chartreuse',
                PYG: 'crimson'
            },
            _tipo: {
                AMP_MONTO: 'red',
                AMP_PLAZO: 'orange',
                OTROS: 'blue',
                REAJUSTE: 'green',
                REN: 'yellow'
            },
            default: '#4CC1E9'
        };
        /* El tamanio del radio del maximo valor encontrado */
        var radius = 25;
        var width = parseInt(document.documentElement.clientWidth) || 0;
        var height = parseInt(document.documentElement.clientWidth / 4) || 0;
        var fill = d3.scale.linear()
                .range(['blue', 'green'])
                .domain([0, 5000]);
        var svg = d3.select(_grafico_modificaciones)
                .append('svg')
                .attr('width', width)
                .attr('height', height)
                .style('display', 'block')
                .style('margin', 'auto');
        modificaciones = getMapeoDeModificaciones(modificaciones, tamanio_circulo);
        var padding = 5;
        var maxRadius = d3.max(_.pluck(modificaciones, 'radius'));
        var maximums = {
            monto: d3.max(_.pluck(modificaciones, 'monto')),
            vigencia_dias: d3.max(_.pluck(modificaciones, 'vigencia_dias'))
        };
        var nodos = svg.selectAll('circle')
                .data(modificaciones)
                .enter().append('circle')
                .attr('class', 'node')
                .attr('cx', function (d) {
                    return d.x;
                })
                .attr('cy', function (d) {
                    return d.x;
                })
                .attr('r', function (d) {
                    return d.radius;
                })
                .style('fill', function (d, i) {
                    return colors['default'];
                })
                .on('mouseover', function (d) {
                    mostrarTooltip.call(this, d);
                })
                .on('mouseout', function (d) {
                    removerTooltip();
                })
                .on('click', function (d) {
                    graficar_modificacion_plazos(d.id_contrato);
                    graficar_modificacion_monto(d.id_contrato);
                    graficar_gant(d.id_contrato);
                });

        var force = d3.layout.force();
        cambiarColor(color);
        dibujar(agrupamiento);
    }
}

/* Grafica PLAZOS */
function graficar_modificacion_plazos(id_contrato) {
    $(_grafico_plazos).empty();

    /* Recuperamos los datos del contrato. Debemos obtener en la columna value el plazo correspondiente al contrato */
    d3.json('/contratos?id=' + id_contrato + '&value=plazo' + top_registros, function (contrato) {
        if (contrato.length) {

            d3.json('/modificaciones?id_contrato=' + id_contrato + '&value=plazo' + top_registros, function (modificaciones) {
                /* Si se trajo algun registro */
                if (modificaciones.length) {
                    var width = parseInt(window.innerWidth / 4) || 0;
                    var height = parseInt(window.innerWidth / 4) || 0;
                    var data_para_graficar = contrato.concat(modificaciones);
                    var r = parseInt(width * 2 / 5);
                    var s = 0;

                    for (var i in data_para_graficar) {
                        data_para_graficar[i].value = parseInt(data_para_graficar[i].value)
                        s = s + data_para_graficar[i].value;
                    }
                    ;

                    var color = d3.scale.category10();

                    var canvas = d3.select(_grafico_plazos)
                            .append('svg')
                            .attr('width', width)
                            .attr('height', height)
                            .style('display', 'block')
                            .style('margin', 'auto');

                    var tooltip = d3.select('body').append('div')
                            .style('position', 'absolute')
                            .style('z-index', '10')
                            .style('visibility', 'hidden')
                            .style('color', 'white').style('padding', '8px')
                            .style('background-color', 'rgba(0, 0, 0, 0.75)')
                            .style('border-radius', '6px')
                            .style('font', '12px sans-serif')
                            .text('tooltip');

                    var group = canvas.append('g')
                            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')

                    var arc = d3.svg.arc()
                            .innerRadius(parseInt(width / 5))
                            .outerRadius(r);

                    var pie = d3.layout.pie()
                            .value(function (d) {
                                return d.value;
                            });

                    var arcs = group.selectAll('.arc')
                            .data(pie(data_para_graficar))
                            .enter()
                            .append('g')
                            .attr('class', 'arc');

                    arcs.append('path')
                            .attr('d', arc)
                            .attr('fill', function (d) {
                                return color(d.data.vigencia_dias)
                            });

                    arcs.append('text')
                            .attr('transform', function (d) {
                                return 'translate(' + arc.centroid(d) + ')'
                            })
                            .attr('text-anchor', 'middle')
                            .attr('font-size', '1.5em')
                            .text(function (d) {
                                if (d.data.value) {
                                    return d.data.value;
                                }
                            });

                    arcs.attr('cursor', 'pointer')
                            .style('stroke', 'black')
                            .style('stroke-width', 0.5)
                            .on('mouseover', mouseover)
                            .on('mouseout', mouseout)
                            .on('mousemove', mousemove);

                    canvas.append('text')
                            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')
                            .text(s + ' días')
                            .attr('font-size', '1.5em')
                            .attr('text-anchor', 'middle');



                    function mouseover(p) {
                        
                        tooltip.html(_mostrarTooltip(p.data,(p.startAngle == 0)? true:false,false))
                                .style('visibility', 'visible');
                        d3.select(this).style('stroke-width', '2px');

                    }

                    function mouseout(p) {

                        tooltip.style('visibility', 'hidden');
                        d3.select(this).style('stroke-width', '0.5px');
                    }

                    function mousemove(p) {
                        tooltip.style('top',
                                (d3.event.pageY - 10) + 'px').style('left', (d3.event.pageX + 10) + 'px');
                    }

                } else {
                    alert('No existen modificaciones de plazos para este contrato');
                }

            });
        } else {
            alert('No existe registro para el identificador de contrato proveido');
        }
    });
}

/* Grafica MONTOS */
function graficar_modificacion_monto(id_contrato) {
    $(_grafico_montos).empty();
    /* Recuperamos los datos del contrato. Debemos obtener en la columna value el plazo correspondiente al contrato */
    d3.json('/contratos?id=' + id_contrato + '&value=monto' + top_registros, function (contrato) {

        if (contrato.length) {

            d3.json('/modificaciones?id_contrato=' + id_contrato + '&value=monto' + top_registros, function (modificaciones) {
                /* Si se trajo algun registro */
                if (modificaciones.length) {
                    var width = parseInt(window.innerWidth / 4) || 0;
                    var height = parseInt(window.innerWidth / 4) || 0;

                    var r = parseInt(width * 2 / 5);
                    contrato[0].value = parseInt(contrato[0].value);

                    if (contrato[0]._moneda == 'PYG') {
                        s = contrato[0].value;
                    } else {
                        s = contrato[0].value * anioCambio[contrato.anio];
                        contrato[0].value = s;
                    }

                    for (var i in modificaciones) {
                        modificaciones[i].value = parseInt(modificaciones[i].value);
                        if (modificaciones[i]._moneda != 'PYG') {
                            modificaciones[i].value = modificaciones[i].value * anioCambio[contrato.anio];
                        }
                        s = s + modificaciones[i].value;
                    }
                    ;

                    var data_para_graficar = contrato.concat(modificaciones);

                     var color = d3.scale.category10();

                    var canvas = d3.select(_grafico_montos)
                            .append('svg')
                            .attr('width', width)
                            .attr('height', height)
                            .style('display', 'block')
                            .style('margin', 'auto');

                    var tooltip = d3.select('body').append('div')
                            .style('position', 'absolute')
                            .style('z-index', '10')
                            .style('visibility', 'hidden')
                            .style('color', 'white').style('padding', '8px')
                            .style('background-color', 'rgba(0, 0, 0, 0.75)')
                            .style('border-radius', '6px')
                            .style('font', '12px sans-serif')
                            .text('tooltip');

                    var group = canvas.append('g')
                            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')

                    var arc = d3.svg.arc()
                            .innerRadius(parseInt(r / 2))
                            .outerRadius(r);

                    var pie = d3.layout.pie()
                            .value(function (d) {
                                return d.value;
                            });

                    var arcs = group.selectAll('.arc')
                            .data(pie(data_para_graficar))
                            .enter()
                            .append('g')
                            .attr('class', 'arc');

                    arcs.append('path')
                            .attr('d', arc)
                            .attr('fill', function (d) {
                                return color(d.data.vigencia_dias)
                            });

                    arcs.append('text')
                            .attr('transform', function (d) {
                                return 'translate(' + arc.centroid(d) + ')'
                            })
                            .attr('text-anchor', 'middle')
                            .attr('font-size', '1.5em')
                            .text(function (d) {
                                if (d.data.value) {
                                    return d.data.value;
                                }
                            });

                    arcs.style('cursor', 'pointer')
                            .style('stroke', 'black')
                            .style('stroke-width', 0.5)
                            .on('mouseover', mouseover)
                            .on('mouseout', mouseout)
                            .on('mousemove', mousemove);

                    canvas.append('text')
                            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')
                            .text(s + ' Gs.')
                            .attr('font-size', '1.5em')
                            .attr('text-anchor', 'middle');



                    function mouseover(p) {
                        tooltip.html(_mostrarTooltip(p.data,(p.startAngle == 0)? true:false,false))
                                .style('visibility', 'visible');
                        d3.select(this).style('stroke-width', '2px');

                    }

                    function mouseout(p) {

                        tooltip.style('visibility', 'hidden');
                        d3.select(this).style('stroke-width', '0.5px');
                    }

                    function mousemove(p) {
                        tooltip.style('top',
                                (d3.event.pageY - 10) + 'px').style('left', (d3.event.pageX + 10) + 'px');
                    }

                } else {
                    alert('No existen modificaciones de montos para este contrato');
                }

            });
        } else {
            alert('No existe registro para el identificador de contrato proveido');
        }
    });
}

function graficar_gant(id_contrato){
    $(_grafico_gantt).empty();

    /* Recuperamos los datos del contrato. Debemos obtener en la columna value el plazo correspondiente al contrato */
    d3.json('/contratos?id=' + id_contrato + '&value=gantt' + top_registros, function (contrato) {
        if (contrato.length) {

            d3.json('/modificaciones?id_contrato=' + id_contrato + '&value=gantt' + top_registros, function (modificaciones) {

		var dataSet = contrato.concat(modificaciones);
		var dataInfo = {};
			legendRectSize = 18;
			legendSpacing = 4;
			duration=1000;
			delay = duration / dataSet.length;

		// D3 requires each object to have a unique key for object constancy
		// http://bost.ocks.org/mike/constancy/
		// We can use underscore to create that if we're not sure if they exist in our data
		// Useful if you're updating data quickly (realtime, daily) and need otherwise meaningless uids

		// Also useful for making an associative array for hover info
		for (var datum in dataSet){

			if(_.has(dataSet, datum)){
				var uid = _.uniqueId('a');
				// Add unique id
				dataSet[datum]['uid'] = uid

				// Create associate array
				dataInfo[uid] = dataSet[datum];
			}
		}

		var tooltip = d3.select(_grafico_gantt).append('div')
						.style('position', 'absolute')
						.style('z-index', '10')
						.style('visibility', 'hidden')
						.style('color', 'white')
						.style('padding', '8px')
						.style('background-color','rgba(0, 0, 0, 0.75)')
						.style('border-radius', '6px')
						.style('font', '12px sans-serif')
						.text('tooltip');

		var tipos = [  'Contrato','Ampliación de plazo', 'Ampliación de monto', 
		'Reajuste', 'Renovación','Otros'];

		var tiposToCodigocolorTipoMap = {
			'Contrato' : 'CONTRATO',
			'Ampliación de plazo' : 'AMP_PLAZO',
			'Ampliación de monto' : 'AMP_MONTO',
			'Reajuste' : 'REAJUSTE',
			'Renovación' : 'REN',
			'Otros' : 'OTROS'
		};

		var colorTipoMap = {
			'CONTRATO': '#4cc1e9',
			'AMP_PLAZO' : 'orange',
			'AMP_MONTO' : 'red',
			'REAJUSTE' : 'green',
			'REN' : 'yellow',
			'OTROS' : 'blue'
		};

		var DATA ={
			dt: dataSet
		}

		var window_width = $(document).width();

		// How many projects do you have?
		var prjs = dataSet.length;

		// Toggler
		var toggler = true;

		// Dimensions
		var bar_height = 20;
		var row_height =  bar_height + 10;
		var vertical_padding = 150
		var bar_start_offset = 40
		var w = window_width - 200;
		var h = prjs * row_height + vertical_padding;

		var svg = d3.select('body')
		.append('svg')
		.attr('width', w)  
		.attr('height', h+50);

		function getDate(date) {return new Date(date);}
		function accessStartDate(d) { return getDate(d.startDate); }
		function accessEndDate(d) { return getDate(d.endDate); }

		var min = d3.min(dataSet, accessStartDate);
		var max = d3.max(dataSet, accessEndDate);

		var paddingLeft = 300;
		var paddingTop = 50;

		var xScale = d3.time.scale()
		.domain([min,max]).nice()
		.range([paddingLeft, w]);

		var xAxis = d3.svg.axis()
		.scale(xScale)
		.orient('bottom');

		// Lines
		var line = svg.append('g')
		.selectAll('line')
		.data(xScale.ticks(10))
		.enter().append('line')
		.attr('x1', xScale)
		.attr('x2', xScale)
		.attr('y1', paddingTop + 30)
		.attr('y2', h-50)
		.style('stroke', '#ccc');

		var y = function(i){
			return i*row_height+paddingTop + bar_start_offset;
		}

		labelY = function(i){
			return i * row_height + paddingTop + bar_start_offset + 13;
		}

		drawLegend(svg, h);

		var bar = svg.selectAll('rect')
		.data(DATA.dt, function(key){return key.uid});

		bar.enter().append('rect')
		.attr('fill', '#fff')
		.style('opacity','0.9');

		bar.transition().duration(1500)
                .delay(function(d, i) { return i * delay; })
		.attr('y', function(d, i) { ;return y(i) })
		.attr('x', function(d) { return xScale(getDate(d.startDate))} )
		.attr('width', function(d) { return (xScale(getDate(d.endDate)) - xScale(getDate(d.startDate)));})
		.attr('height', bar_height)
		.style('fill', function(d) {return alternatingColorScale(d,d.uid);''})
		.style('opacity', '1');

		bar
		.attr('cursor', 'pointer')
		.style('stroke', 'black')
		.style('stroke-width', '0.5px')
		.on('mouseover', mouseover)
		.on('mouseout', mouseout)
		.on('mousemove', mousemove);

		var label = svg.selectAll('text')
		.data(DATA.dt, function(key){return key.uid});

		label.enter().append('text')
		.attr('class', 'bar-label')
		.attr('x', paddingLeft-10)
		.attr('y', function(d, i) { return labelY(i); })
		.text(function(d){return d.id});

                // Bottom Axis
                var btmAxis = svg.append('g')
                .attr('transform', 'translate(0,'+(h - 25)+')')
                .attr('class', 'axis')
                .call(xAxis);

                // Top Axis
                var topAxis = svg.append('g')
                .attr('transform', 'translate(0,'+paddingTop+')')
                .attr('class', 'axis')
                .call(xAxis);

                // Hover info
                function mouseover(d) {
                    tooltip.html(_mostrarTooltip(d, (d._tipo == 'CONTRATO') ? true : false, true));  		
                    tooltip.style('visibility', 'visible');
                    d3.select(this).style('stroke-width', '2px');
                }

                function mouseout() {
                    tooltip.style('visibility', 'hidden');
                    d3.select(this).style('stroke-width', '0.5px');
                }
                function mousemove() {
                    tooltip.style('top',
                    (d3.event.pageY-10)+'px').style('left',(d3.event.pageX+10)+'px');
                }

                function alternatingColorScale (d,codigo) {
                    if (codigo){
                        return colorTipoMap[dataInfo[codigo]._tipo];
                    } else {
                        return colorTipoMap[tiposToCodigocolorTipoMap[d]];
                    }

                }

                /* Leyenda de colores */
                function drawLegend(chart, chartHeight){
                    var hor =50;
                    var ver = chartHeight+30;
                    var legend = chart.selectAll('.legend').data(tipos).enter()
                                    .append('g').attr('class', 'legend').attr('transform',
                                                    function(d, i) {
                                                            hor = hor + 150;
                                                            return 'translate(' + hor + ',' + ver + ')';
                                                    });

                    legend.append('rect')
                            .attr('width', legendRectSize)
                            .attr('height', legendRectSize - 10)
                            .style('fill', function(d){return alternatingColorScale(d);})
                            .style('stroke', function(d){return alternatingColorScale(d);});

                    legend.append('text')
                            .attr('x', legendRectSize + legendSpacing)
                            .attr('y', legendRectSize - legendSpacing - 6)
                            .style('font', '12px sans-serif')
                            .text(function(d) { return d; });
                }

            });
        }
    });
}