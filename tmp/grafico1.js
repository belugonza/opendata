/* No se utilizara mas */
opendata.graficos.modificaciones.tamanio_circulo = '';
/* Espacio de nombres para grafico de modificacionesanio */
opendata.graficos.modificaciones = {
    'agrupamiento': '',
    'color': '',
    'categoria': {
        '0': 'Adquisición y Locación de inmuebles. Alquiler de muebles',
        '1': 'Bienes e insumos agropecuario y forestal',
        '2': 'Capacitaciones y Adiestramientos',
        '3': 'Combustibles y Lubricantes',
        '4': 'Construcción, Restauración, Reconstrucción o Remodelación y Reparación de Inmuebles',
        '5': 'Consultorías, Asesorías e Investigaciones. Estudios y Proyectos de inversión',
        '6': 'Elementos e insumos de limpieza',
        '7': 'Equipos Militares y de Seguridad. Servicio de Seguridad y Vigilancia',
        '8': 'Equipos, accesorios y programas computacionales, de oficina, educativos, de imprenta, de comunicación y señalamiento',
        '9': 'Equipos, Productos e instrumentales Médicos y de Laboratorio. Servicios asistenciales de salud',
        '10': 'Maquinarias, Equipos y herramientas mayores - Equipos de transporte',
        '11': 'Materiales e insumos eléctricos, metálicos y no metálicos, Plásticos, cauchos. Repuestos, herramientas, cámaras y cubiertas.',
        '12': 'Minerales',
        '13': 'Muebles y Enseres',
        '14': 'Pasajes y Transportes',
        '15': 'Productos Alimenticios',
        '16': 'Productos quimicos',
        '17': 'Publicidad y Propaganda',
        '18': 'Seguros',
        '19': 'Servicios de ceremonial, gastronomico y funerarios',
        '20': 'Servicios de Limpiezas, Mantenimientos y reparaciones menores y mayores de Instalaciones, Maquinarias y Vehículos',
        '21': 'Servicios Técnicos',
        '22': 'Textiles, vestuarios y calzados',
        '23': 'Utensilios de cocina y comedor, Productos de Porcelana, vidrio y loza',
        '24': 'Utiles de oficina, Productos de papel y cartón e impresos'
    },
    'graficar': function () {
        /* Limpiamos los graficos de montos, plazos y gantt */
        private.reset();
        /* Parametros para el grafico inicial */
        var params = {
            'id_contrato': $(opendata.graficos.globals.textbox_id_contrato).val(),
            'categoria': $(opendata.graficos.globals.select2_categoria_contrato).select2("val"),
            'cantidad': $(opendata.graficos.globals.combobox_cantidad_modificaciones).val(),
            'anio': $(opendata.graficos.globals.select2_anio).select2("val"),
            'convocantes': $(opendata.graficos.globals.select2_convocante).select2("val")
        }
        /* Llamada ajax para graficar la primera vista */
        $.ajax({
            'type': 'POST',
            'url': '/modificaciones',
            'data': params,
            'dataType': 'json',
            'success': function (datos) {
                private.crear_grafico(datos);
            }
        })
    },
    'crear_grafico': function (modificaciones) {
        /* Setea la ubicacion de cada modificacion */
        function getMapeoDeModificaciones(modificaciones, nombre_del_campo) {
            /* Obtenemos el maximo valor del campo de entre las modificaciones */
            var max = d3.max(_.pluck(modificaciones, nombre_del_campo));
            if (!max) {
                max = 1;
            }
            /* Agregamos coordenadas de centro y radio a cada modificacion */
            for (var i in modificaciones) {
                /* Se calcula el radio que tendra esta modificacion en proporcionalmente al tamanio del maximo.
                 * El valor minimo sera 15 en caso de no tener especificado el nombre del campo */
                modificaciones[i].radius = (nombre_del_campo != '') ? radius * (modificaciones[i][nombre_del_campo] / max) : 15;
                modificaciones[i].x = modificaciones[i].x ? modificaciones[i].x : Math.random() * width;
                modificaciones[i].y = modificaciones[i].y ? modificaciones[i].y : Math.random() * height;
            }

            return modificaciones;
        }

        /* Categoriza al monto de la modificacion en ALTO, MEDIO, BAJO */
        function getCategoriaDeMonto(modificacion) {
            var max = d3.max(_.pluck(modificaciones, 'monto'));
            if (!max) {
                max = 1;
            }
            var val = modificacion['monto'] / max;
            if (val > 0.4)
                return 'Mayores';
            else if (val > 0.1)
                return 'Medios';
            else
                return 'Bajos';
        }

        /* Funcion para dibujar de acuerdo a la agrupacion */
        function dibujar(criterio) {
            var centros = getCentros(criterio, [width, height]);
            force.on('tick', marcar(centros, criterio));
            etiquetar_nodos(centros);
            force.start();
        }

        /* Funcion que obtiene los centros para las modificaciones */
        function getCentros(criterio, tamanio_lienzo) {
            /* La clave 'name' es para la etiqueta */
            var centros = _.uniq(_.pluck(modificaciones, criterio)).map(function (d) {
                return {name: d, value: 1};
            });
            /* NO SE PARA QUE SE HACE ESTO */
            var map = d3.layout.treemap()
                    .size(tamanio_lienzo)
                    .ratio(1 / 1)
                    .nodes({children: centros});
            return centros;
        }

        /* No se exactamente que hace */
        function marcar(centros, criterio) {
            var focos = {};
            for (i in centros) {
                focos[centros[i].name] = centros[i];
            }

            /* Funcion utilizada en el retorno */
            function colisionar(alpha) {
                var quadtree = d3.geom.quadtree(modificaciones);
                return function (d) {
                    var r = d.radius + maxRadius + padding,
                            nx1 = d.x - r,
                            nx2 = d.x + r,
                            ny1 = d.y - r,
                            ny2 = d.y + r;
                    quadtree.visit(function (quad, x1, y1, x2, y2) {
                        if (quad.point && (quad.point !== d)) {
                            var x = d.x - quad.point.x,
                                    y = d.y - quad.point.y,
                                    l = Math.sqrt(x * x + y * y),
                                    r = d.radius + quad.point.radius + padding;
                            if (l < r) {
                                l = (l - r) / l * alpha;
                                d.x -= x *= l;
                                d.y -= y *= l;
                                quad.point.x += x;
                                quad.point.y += y;
                            }
                        }
                        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                    });
                };
            }

            return function (e) {
                for (i in modificaciones) {
                    var o = modificaciones[i];
                    var f = focos[o[criterio]];
                    o.y += ((f.y + (f.dy / 2)) - o.y) * e.alpha;
                    o.x += ((f.x + (f.dx / 2)) - o.x) * e.alpha;
                }
                nodos.each(colisionar(.11))
                        .attr('cx', function (d) {
                            return d.x;
                        })
                        .attr('cy', function (d) {
                            return d.y;
                        });
            }

        }

        /* Funcion para setear las etiquetas */
        function etiquetar_nodos(centros) {
            svg.selectAll('.label').remove();
            svg.selectAll('.label')
                    .data(centros)
                    .enter()
                    .append('text')
                    .attr('class', 'label')
                    .text(function (d) {
                        return etiqueta[d.name];
                    })
                    .attr('transform', function (d) {
                        return 'translate(' + (d.x + (d.dx / 2)) + ', ' + (d.y + 20) + ')';
                    });
        }

        function cambiarColor(criterio) {
            d3.selectAll('circle')
                    .transition()
                    .style('fill', function (d) {
                        return criterio ? colors[criterio][d[criterio]] : colors['default'];
                    })
                    .duration(1000);
            $('.colors').empty();
            if (criterio) {
                $('.colors').append('<div class="col-xs-1"><label style="margin-left:50px;">Colores:</label></div>');
                for (var i in colors[criterio]) {
                    $('.colors').append('<div class="col-xs-2 color-legend" style="background:' + colors[criterio][i] + ';">' + codigoToTipoModificacion[i] + '</div>')
                }
            }
        }

        var tooltip = d3.select('body').append('div')
                .style('position', 'absolute')
                .style('z-index', '10')
                .style('visibility', 'hidden')
                .style('color', 'white').style('padding', '8px')
                .style('background-color', 'rgba(0, 0, 0, 0.75)')
                .style('border-radius', '6px')
                .style('font', '12px sans-serif')
                .text('tooltip');
        /* Funcion para mostrar el tooltip */
        function mouseover(modificacion) {
            tooltip.html(_mostrarTooltip(modificacion, false, false))
                    .style('visibility', 'visible')
                    .style('stroke-width', '2px');
        }
        /* Funcion para remover el tooltip */
        function mouseout(modificacion) {
            tooltip.style('visibility', 'hidden')
                    .style('stroke-width', '0.5px');
        }
        /* Funcion para acompañar el tooltip */
        function mousemove(modificacion) {
            tooltip.style('top',
                    (d3.event.pageY - 10) + 'px').style('left', (d3.event.pageX + 10) + 'px');
        }

        /* Cambios en filtros de contratos */
        $(_textbox_id_contrato).change(function () {
            start();
        });
        /* Cambios en filtros de modificaciones */
        $(_combobox_cantidad_modificaciones).change(function () {
            start();
        });
        $(_select2_anio).change(function () {
            start();
        });
        $(_combobox_agrupar).change(function () {
            agrupamiento = this.value;
            dibujar(agrupamiento);
        });
        $(_combobox_tamanio_circulo).change(function () {
            var criterio = this.value;
            var max = d3.max(_.pluck(modificaciones, criterio));
            if (!max) {
                max = 1;
            }
            d3.selectAll('circle')
                    .data(getMapeoDeModificaciones(modificaciones, this.value))
                    .transition()
                    .attr('r', function (d, i) {
                        return criterio ? (radius * (modificaciones[i][criterio] / max)) : 15;
                    })
                    .attr('cx', function (d) {
                        return d.x;
                    })
                    .attr('cy', function (d) {
                        return d.y;
                    })
                    .duration(1000);
            tamanio_circulo = this.value;
            force.start();
        });
        $(_combobox_color).change(function () {
            color = this.value;
            cambiarColor(color);
        });
        var colors = {
            _moneda: {
                USD: 'chartreuse',
                PYG: 'crimson'
            },
            _tipo: {
                AMP_PLAZO: 'orange',
                AMP_MONTO: 'red',
                REAJUSTE: 'green',
                REN: 'yellow',
                OTROS: 'blue'
            },
            default: '#4CC1E9'
        };
        /* El tamanio del radio del maximo valor encontrado */
        var radius = 25;
        var width = parseInt(document.documentElement.clientWidth) || 0;
        var height = parseInt(document.documentElement.clientWidth / 4) || 0;
        var fill = d3.scale.linear()
                .range(['blue', 'green'])
                .domain([0, 5000]);
        $(_grafico_modificaciones).empty();
        var svg = d3.select(_grafico_modificaciones)
                .append('svg')
                .attr('width', width)
                .attr('height', height)
                .style('display', 'block')
                .style('margin', 'auto');
        modificaciones = getMapeoDeModificaciones(modificaciones, tamanio_circulo);
        var padding = 5;
        var maxRadius = d3.max(_.pluck(modificaciones, 'radius'));
        var maximums = {
            monto: d3.max(_.pluck(modificaciones, 'monto')),
            vigencia_dias: d3.max(_.pluck(modificaciones, 'vigencia_dias'))
        };
        var nodos = svg.selectAll('circle')
                .data(modificaciones)
                .enter().append('circle')
                .attr('class', 'node')
                .attr('cx', function (d) {
                    return d.x;
                })
                .attr('cy', function (d) {
                    return d.x;
                })
                .attr('r', function (d) {
                    return d.radius;
                })
                .style('fill', function (d, i) {
                    return colors['default'];
                })
                .on('mouseover', function (d) {
                    mouseover(d);
                })
                .on('mouseout', function (d) {
                    mouseout();
                })
                .on('mousemove', function (d) {
                    mousemove();
                })
                .on('click', function (d) {
                    graficar_modificacion_plazos(d.id_contrato);
                    graficar_modificacion_monto(d.id_contrato);
                    graficar_gant(d.id_contrato);
                });
        var force = d3.layout.force();
        cambiarColor(color);
        dibujar(agrupamiento);
    },
    'reset': function () {
        $(opendata.graficos.globals.grafico_modificaciones).empty();
        $(opendata.graficos.globals.titulo_montos).empty();
        $(opendata.graficos.globals.grafico_montos).empty();
        $(opendata.graficos.globals.titulo_plazos).empty();
        $(opendata.graficos.globals.grafico_plazos).empty();
        $(opendata.graficos.globals.titulo_gantt).empty();
        $(opendata.graficos.globals.grafico_gantt).empty();
    }
}